const withCSS = require('@zeit/next-css')
const withOffline = require("next-offline")
const path = require("path")
const { parsed: localEnv } = require('dotenv').config()

const nextConfig = {
  target: 'serverless',
  generateInDevMode: true,
  workboxOpts: {
    swDest: 'static/service-worker.js',
    runtimeCaching: [
      {
        urlPattern: /^https?.*/,
        handler: 'NetworkFirst',
        options: {
          cacheName: 'https-calls',
          networkTimeoutSeconds: 15,
          expiration: {
            maxEntries: 150,
            maxAgeSeconds: 30 * 24 * 60 * 60
          },
          cacheableResponse: {
            statuses: [0, 200]
          }
        }
      }
    ]
  },
  webpack(config, { dev }) {
    config.plugins.push(new webpack.EnvironmentPlugin(localEnv))
    if (dev) {
      config.module.rules.push(
        {
          test: /\.css$/,
          use: [
            'style-loader',
            { loader: 'css-loader', options: { importLoaders: 1 } },
            { 
              loader: 'postcss-loader',
              options: {
                ident: 'postcss',
                plugins: [
                  require('tailwindcss'),
                  require('autoprefixer')
                ],
              },
            }
          ]
        },
      )
    }
    return config
  },
}

function HACK_removeMinimizeOptionFromCssLoaders(config) {
  console.warn(
    'HACK: Removing `minimize` option from `css-loader` entries in Webpack config',
  );
  config.module.rules.forEach(rule => {
    if(Array.isArray(rule.use)) {
      rule.use.forEach(u => {
        if(u.loader === 'css-loader' && u.options) {
          delete u.options.minimize;
        }
      });
    }
  });
}

module.exports = withOffline(
  withCSS({
    cssModules: false,
    cssLoaderOptions: {
      importLoaders: 1
    },
    webpack(config) {
      HACK_removeMinimizeOptionFromCssLoaders(config)
      config.resolve.modules.push(path.resolve('./'))
      return config
    },
    nextConfig,
    env: {
      OMH_PUBLIC_API_KEY: process.env.OMH_PUBLIC_API_KEY,
      omhweb_auth: process.env.omhweb_auth
    }
  })
)
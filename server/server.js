const express = require('express')
const next = require('next')
const { parse } = require('url')
const { join } = require('path')

const port = parseInt(process.env.PORT, 10) || 5000
const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()

app
  .prepare()
  .then(() => {

    const server = express()

    server.get('/:country/:language/property-calculator/affordability-calculator', (req, res) => {
      app.render(req, res, "/:country/:language/property-calculator/affordability-calculator")
    })
    server.get('/:country/:language/property-calculator/buyers-stamp-duty', (req, res) => {
      app.render(req, res, "/:country/:language/property-calculator/buyers-stamp-duty")
    })
    server.get('/:country/:language/property-calculator/sellers-stamp-duty', (req, res) => {
      app.render(req, res, "/:country/:language/property-calculator/sellers-stamp-duty")
    })
    server.get('/:country/:language/home', (req, res) => {
      app.render(req, res, "/:country/:language/home")
    })
    server.get('/:country/:language/new-property-launch/:slug', (req, res) => {
      app.render(req, res, "/:country/:language/new-property-launch/:slug")
    })
    server.get('/:country/:language/prelaunch', (req, res) => {
      app.render(req, res, "/:country/:language/prelaunch")
    })
    server.get('/:country/:language/newlaunches', (req, res) => {
      app.render(req, res, "/:country/:language/newlaunches")
    })

    server.get('/*', (req, res) => {
      const parsedUrl = parse(req.url, true)
      const { pathname } = parsedUrl

      console.log(req.url)
      console.log('-------------')

      if(pathname === '/service-worker.js') {
        const filePath = join(__dirname, '.next', pathname)
        app.serveStatic(req, res, filePath)
      } else {
        handle(req, res, parsedUrl)
      }
    })

    server.listen(port, err => {
      if(err) throw err
      console.log(`> Ready on http://localhost:${port}`)
    })
  })
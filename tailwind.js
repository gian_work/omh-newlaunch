module.exports = {
  important: true,
  theme: {
    fontFamily: {
      body: ['Public sans', 'sans-serif'],
      display: ['Public sans', 'sans-serif']
    },
    extend: {
      colors: {
        cyan: '#9cdbff',
        gray: '#7E7E7E',
        lightGray: '#F7F7F7',
        lighterGray: '#BDBDBD',
        mediumGray: '#EEEEEE',
        mediumDarkGray: '#A8A8A8',
        darkGray: '#484848',
        orange: '#EE620F',
        error: '#f44336',
        black: '#000',
        white: '#FFF',
        D8D8D8: '#D8D8D8',
        343434: '#343434',
        151515: '#151515',
        D1D1D1: '#D1D1D1',
        E1E1E1: '#E1E1E1',
        F7F7F7: '#F7F7F7',
      },
      borderColor: {
        'transparentWhite': 'rgba(255, 255, 255, 0.3)',
        'E8E8E8': '#E8E8E8',
        'E1E1E1': '#E1E1E1'
      },
      borderRadius: {
        btn: '4px',
        '4': '4px'
      },
      margin: {
        '5': '5px',
        '8': '8px',
        '10': '10px',
        '15': '15px',
        '18': '18px',
        '20': '20px',
        '22': '22px',
        '25': '25px',
        '30': '30px',
        '32': '32px',
        '35': '35px',
        '45': '45px',
        '70': '70px',
        '96': '24rem',
        '128': '32rem',
      },
      padding: {
        '3': '3px',
        '5': '5px',
        '10': '10px',
        '15': '15px',
        '20': '20px',
        '25': '25px',
        '35': '35px',
        '40': '40px',
        '45': '45px',
        '60': '60px',
        '80': '80px',
        '100': '100px',
        '180': '180px',
        'headerNav': '17.5px'
      },
      fontFamily: {
        'body': ['Public Sans VF']
      },
      fontSize: {
        '12': '12px',
        '11': '11px',
        '13': '13px',
        '14': '14px',
        '15': '15px',
        '16': '16px',
        '17': '17px', 
        '18': '18px',
        '19': '19px',
        '20': '20px', 
        '24': '24px', 
        '26': '26px', 
        '30': '30px',
        '40': '40px', 
        '48': '48px', 
      },
      lineHeight : {
        '27' : '27px',
        '17' : '17px',
        '125%' : '125%',
        '130%' : '130%',
        '140%' : '140%'
      },
      height: {
        '25': '25px',
        '40': '40px',
        '50': '50px',
        'btn': '44px',
        'headerBtn': '40px',
        'headerSm': '56px',
        'headerMobile': '70px',
        'headerMobileGuest': '140px',
        '200%' : '200%',
      },
      width: {
        'icon': '18px',
        '25': '25px',
        '40': '40px',
        '50': '50px',
        '110': '110px',
        '200': '200px',
        '460': '460px',
        'loginToggle': '500px',
        '200%' : '200%',
      },
      maxWidth: {
        'defaultWrapper': '980px',
      },
      inset : {
        '50' : '50%',
        '-50' : '-50%'
      },
    }
  },
  variants: {
    opacity: ['responsive', 'hover']
  },
}
export const bannerConfig = {
  Banner: [
    {
      imageUrlDesktop: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      imageUrlTablet: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      imageUrlMobile: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      bannerTitleDesktop: 'The Riverfront Residences Desktop 1',
      bannerSubTitleDesktop: 'A rare parcel of land with 200 meters of riverfront living designed by award winning architect ADDP',
      bannerTitleMobile: 'The Riverfront Residences Desktop 1',
      bannerSubTitleMobile: 'A rare parcel of land with 200 meters of riverfront living designed by award winning architect ADDP',
      bannerURL: '#',
      bannerButton: 'Discover project',
    },
    {
      imageUrlDesktop: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      imageUrlTablet: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      imageUrlMobile: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      bannerTitleDesktop: 'The Riverfront Residences Desktop 2',
      bannerSubTitleDesktop: 'A rare parcel of land with 200 meters of riverfront living designed by award winning architect ADDP',
      bannerTitleMobile: 'The Riverfront Residences Desktop 2',
      bannerSubTitleMobile: 'A rare parcel of land with 200 meters of riverfront living designed by award winning architect ADDP',
      bannerURL: '#',
      bannerButton: 'Discover project',
    },
    {
      imageUrlDesktop: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      imageUrlTablet: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      imageUrlMobile: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      bannerTitleDesktop: 'The Riverfront Residences Desktop 3',
      bannerSubTitleDesktop: 'A rare parcel of land with 200 meters of riverfront living designed by award winning architect ADDP',
      bannerTitleMobile: 'The Riverfront Residences Desktop 3',
      bannerSubTitleMobile: 'A rare parcel of land with 200 meters of riverfront living designed by award winning architect ADDP',
      bannerURL: '#',
      bannerButton: 'Discover project',
    },
    {
      imageUrlDesktop: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      imageUrlTablet: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      imageUrlMobile: 'https://api.omh.app/store/cms/media/singapore/newlaunch/banner.png',
      bannerTitleDesktop: 'The Riverfront Residences Desktop 4',
      bannerSubTitleDesktop: 'A rare parcel of land with 200 meters of riverfront living designed by award winning architect ADDP',
      bannerTitleMobile: 'The Riverfront Residences Desktop 4',
      bannerSubTitleMobile: 'A rare parcel of land with 200 meters of riverfront living designed by award winning architect ADDP',
      bannerURL: '#',
      bannerButton: 'Discover project',
    },
  ]
}

export const districtConfig = {
  District: [
    {
      image: 'https://api.omh.app/store/cms/media/singapore/newlaunch/district-1.png',
      url: '#newlaunch-listings',
      title: 'Core Central Region',
      subtitle: 'Singapore’s Food District'
    },
    {
      image: 'https://api.omh.app/store/cms/media/singapore/newlaunch/district-2.png',
      url: '#newlaunch-listings',
      title: 'Rest of Central Region',
      subtitle: 'Business Districts'
    },
    {
      image: 'https://api.omh.app/store/cms/media/singapore/newlaunch/district-3.png',
      url: '#newlaunch-listings',
      title: 'Singapore’s Food District',
      subtitle: 'Outside Region'
    },
  ]
}

export const uspConfig = {
  USP: [
    {
      image: 'https://api.omh.app/store/cms/media/singapore/newlaunch/image 7.png',
      title: 'Core Central Region',
      excerpt: 'Singapore’s Food District'
    },
    {
      image: 'https://api.omh.app/store/cms/media/singapore/newlaunch/image 7.png',
      title: 'Core Central Region',
      excerpt: 'Singapore’s Food District'
    },
    {
      image: 'https://api.omh.app/store/cms/media/singapore/newlaunch/image 7.png',
      title: 'Core Central Region',
      excerpt: 'Singapore’s Food District'
    },
  ]
}

export const blogConfig = {
  Blogs: [
    {
      image: 'https://api.omh.app/store/cms/media/singapore/Calculator_Meta_image.png',
      category: 'TOP 10 IN THE MARKET',
      title: 'On the lookout for Singapore’s hottest properties right now',
      excerpt: 'Residents in the area benefit greatly from the convenience and access to establishments',
      cta: 'See top 10 listings',
      url: '#',
    },
    {
      image: 'https://api.omh.app/store/cms/media/singapore/Calculator_Meta_image.png',
      category: 'TOP 10 IN THE MARKET',
      title: 'On the lookout for Singapore’s hottest properties right now',
      excerpt: 'Residents in the area benefit greatly from the convenience and access to establishments',
      cta: 'See top 10 listings',
      url: '#',
    },
    {
      image: 'https://api.omh.app/store/cms/media/singapore/Calculator_Meta_image.png',
      category: 'TOP 10 IN THE MARKET',
      title: 'On the lookout for Singapore’s hottest properties right now',
      excerpt: 'Residents in the area benefit greatly from the convenience and access to establishments',
      cta: 'See top 10 listings',
      url: '#',
    },
  ]
}

export const newlaunchConfig = {
  Newlaunch: [
    {
      featured_image: 'https://api.omh.app/store/cms/media/singapore/Calculator_Meta_image.png',
      badge: 'North Central',
      slogan: 'Gramercy Park @ Grange Road',
      description: 'It promises tranquility despite being in the city, with the lushness of its greenery and size owners would enjoy',
      slug: '#',
    },
    {
      featured_image: 'https://api.omh.app/store/cms/media/singapore/Calculator_Meta_image.png',
      badge: 'North Central',
      slogan: 'Gramercy Park @ Grange Road',
      description: 'It promises tranquility despite being in the city, with the lushness of its greenery and size owners would enjoy',
      slug: '#',
    },
    {
      featured_image: 'https://api.omh.app/store/cms/media/singapore/Calculator_Meta_image.png',
      badge: 'North Central',
      slogan: 'Gramercy Park @ Grange Road',
      description: 'It promises tranquility despite being in the city, with the lushness of its greenery and size owners would enjoy',
      slug: '#',
    },
    {
      featured_image: 'https://api.omh.app/store/cms/media/singapore/Calculator_Meta_image.png',
      badge: 'North Central',
      slogan: 'Gramercy Park @ Grange Road',
      description: 'It promises tranquility despite being in the city, with the lushness of its greenery and size owners would enjoy',
      slug: '#',
    },
    {
      featured_image: 'https://api.omh.app/store/cms/media/singapore/Calculator_Meta_image.png',
      badge: 'North Central',
      slogan: 'Gramercy Park @ Grange Road',
      description: 'It promises tranquility despite being in the city, with the lushness of its greenery and size owners would enjoy',
      slug: '#',
    },
    {
      featured_image: 'https://api.omh.app/store/cms/media/singapore/Calculator_Meta_image.png',
      badge: 'North Central',
      slogan: 'Gramercy Park @ Grange Road',
      description: 'It promises tranquility despite being in the city, with the lushness of its greenery and size owners would enjoy',
      slug: '#',
    },
  ]
}


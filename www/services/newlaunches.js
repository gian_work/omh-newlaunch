
import request from './request'
import API from './endpoints'

function getNewLaunchesList(values) {
  return request({
    url: API.GET_PROJECT_DETAILS,
    method: "GET",
    headers: {
      'OmhWeb-Auth': "0uYOdpAFA8WW9YkyD6SKdPOtD9zZQHRH98pxWRTuvEYcKStpc87yGUbCvb5lN8mu",
    },
    params: {
      ...values
    }
  })
}

const NewLaunchesList = {
  getNewLaunchesList,
}

export default NewLaunchesList
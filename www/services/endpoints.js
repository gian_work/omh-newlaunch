const HOST_CMS = 'https://cms.staging.ohmyhome.io/api'
// const HOST_API = 'https://staging.ohmyhome.com/ms'

const API = {
  BASE_URL: HOST_CMS,
  GET_PROJECT_DETAILS: '/new-launches',
}

export default API
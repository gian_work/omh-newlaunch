import * as React from "react"
import UspCard, { UspProps } from './index'

interface UspGroupProps {
	USP: UspProps[]
}


const UspGroup = ({ USP }: UspGroupProps) => {

  return (
    <div className="usp-wrapper block md:flex justify-between relative w-full">
			{
				USP.map((USP, index) => (
						<UspCard 
							key={index} {...USP}
							image = {USP.image}
							title = {USP.title}
							excerpt = {USP.excerpt}
						/>
					)
				)
			}
			<style jsx>{`
				.usp-wrapper {
					overflow-x: auto;
					white-space: nowrap;
					min-width: 280px;
				}
				.usp-image_wrapper:before {
					content: "1";
					position: absolute;
					left: 0;
					right: 0;
					bottom: 0;
					top: 0;
					text-indent: -999999px;
					overflow: hidden;
					background: linear-gradient(0.26deg, #000000 29.56%, rgba(255, 255, 255, 0) 43.52%, rgba(255, 255, 255, 0) 92.54%);
					opacity: 0.3;
				}
				@media screen and (min-width: 768px) { 
					.usp-wrapper {
						min-width: 960px;
					}
				}
				@media screen and (min-width: 500px) { 
					.usp-wrapper {			
						min-width: 340px;
					}
				}
      `}</style>
    </div>
  )
}

export default UspGroup
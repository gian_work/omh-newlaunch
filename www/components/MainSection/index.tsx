import * as React from "react"

interface SectionProps {
	background?: string,
	marginDesktop?: string,
	marginMobile?: string,
	titlePosition?: string,
  title?: string,
  subtitle?: string,
  url?: string,
  button?: string,
	children?: any,
}


const MainSection = ({
	background,
	marginDesktop,
	marginMobile,
	titlePosition,
	title,
	subtitle,
	url,
	button,
	children
}: SectionProps) => {

  return (
    <div className="section-wrapper relative w-full">
			<div className="section-container mx-auto">
				{ title &&
					<div className="section-header">
						<h2>{title}</h2>
						<span>{subtitle}</span>
					</div>
				}
				{ children &&
					<div className="section-content">
						{children}
					</div>
				}
				{ button &&
					<div className="section-footer">
						<a href={url}>{button}</a>
					</div>
				}
			</div>
			<style jsx>{`
				.section-wrapper {
					margin: ${marginMobile ? marginMobile : 'auto'};
					background: ${background ? background : 'transparent'};
					padding: 0 20px;
				}
				.section-container {
					max-width: 1248px;
				}
				.section-header {
					margin: 0 0 34px;
					text-align: ${titlePosition ? titlePosition : 'left'};
				}
				.section-header h2 {
					font-size: 28px;
					line-height: 33px;
					margin: 0 0 12px;
					color: #1C1C1C;
				}
				.section-header span {
					display: block;
					font-size: 18px;
					line-height: 21px;
					color: #5F5F5F;
				}
				.section-content {
					margin: 0 0 69px;
				}
				.section-footer {
					display: flex;
					justify-content: center;
				}
				.section-footer a {
					border: 1px solid #E86225;
					box-sizing: border-box;
					border-radius: 5px;
					font-weight: 600;
					font-size: 14px;
					line-height: 14px;
					padding: 15px 25px;
					color: #E86225;
					transition: .2s ease-in-out;
				}
				.section-footer a:hover {	
					background-color:#E86225;
					color: #FFF;
				}
				@media screen and (min-width: 768px) {
					.section-wrapper {
						margin: ${marginDesktop ? marginDesktop : 'auto'};
						padding: 0 40px;
					}
				}
      `}</style>
    </div>
  )
}

export default MainSection
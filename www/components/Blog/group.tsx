import * as React from "react"
import BlogCard, { BlogProps } from './index'


interface BlogGroupProps {
	Blogs: BlogProps[]
}


const BlogGroup = ({ Blogs }: BlogGroupProps) => {

  return (
    <div className="blog-wrapper block md:flex justify-between relative w-full">
			{
				Blogs.map((Blogs, index) => (
						<BlogCard 
							key={index} {...Blogs}
							image = {Blogs.image}
							title = {Blogs.title}
							excerpt = {Blogs.excerpt}
						/>
					)
				)
			}
			<style jsx>{`
				.blog-wrapper {
				}
				.blog-image_wrapper:before {
					content: "1";
					position: absolute;
					left: 0;
					right: 0;
					bottom: 0;
					top: 0;
					text-indent: -999999px;
					overflow: hidden;
					background: linear-gradient(0.26deg, #000000 29.56%, rgba(255, 255, 255, 0) 43.52%, rgba(255, 255, 255, 0) 92.54%);
					opacity: 0.3;
				}
				@media screen and (min-width: 768px) { 
					.blog-wrapper {
					}
				}
				@media screen and (min-width: 500px) { 
					.blog-wrapper {			

					}
				}
      `}</style>
    </div>
  )
}

export default BlogGroup
import * as React from "react"

export interface BlogProps {
	image?: string,
	category?: string,
	title?: string,
	excerpt?: string,
	cta?: string,
	url?: string,
}


const BlogCard = ({ image, category, title, excerpt, url, cta }: BlogProps) => (
	<div className="blog-container block relative w-full overflow-hidden">
		<div className="blog-image_wrapper">
			<img src={image} alt={title}/>
		</div>
		<div className="blog-content_wrapper text-left">
			<span className="block">{category}</span>
			<h3>{title}</h3>
			<p>{excerpt}</p>
			<a href={url} className="flex justify_start items-center align-left">
				{cta}
				<svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
					<path d="M10.5 5.92969L9 7.42969L13.5703 12L9 16.5703L10.5 18.0703L16.5703 12L10.5 5.92969Z" fill="#E86225"/>
				</svg>
			</a>
		</div>
		<style jsx>{`
				.blog-container {
					max-width: 400px;
					box-sizing: border-box;
					border: 1px solid #E0E0E0;
					border-radius: 5px;
					margin: 0 auto 32px;

				}
				.blog-container:last-child {
					margin: 0;
				}
				.blog-image_wrapper img {
					width: 100%;
				}
				.blog-content_wrapper {
					padding: 19px 17px;
				}
				.blog-content_wrapper span {
					font-weight: 600;
					font-size: 12px;
					line-height: 14px;
					margin: 0 0 4px;
				}
				.blog-content_wrapper h3{
					color: #1C1C1C;
					font-weight: 600;
					font-size: 20px;
					line-height: 26px;
					margin: 0 0 12px;
				}
				.blog-content_wrapper p{
					color: #5F5F5F;
					font-size: 14px;
					line-height: 20px;
					color: #5F5F5F;
					margin: 0 0 15px;
				}
				.blog-content_wrapper a:before {
					content: "1";
					position: absolute;
					left: 0;
					right: 0;
					top: 0;
					bottom: 0;
					text-indent: -99999px;
					oveflow: hidden;
				}
				.blog-content_wrapper a {
					font-weight: 500;
					font-size: 14px;
					line-height: 14px;
					color: #E86225;
				}
				@media screen and (min-width: 768px) { 
					.blog-container {
						margin: 0 22px 0 0;
					}
					.blog-content_wrapper span {
						font-size: 13px;
						line-height: 15px;
					}
					.blog-content_wrapper h3{
						font-size: 20px;
						line-height: 26px;
						margin: 0 0 9px;
					}
					.blog-content_wrapper p{
						font-size: 15px;
						line-height: 22px;
						margin: 0 0 18px;
					}
					.blog-content_wrapper a {
						font-weight: 500;
						font-size: 14px;
						line-height: 14px;
						color: #E86225;
					}
				}
				@media screen and (min-width: 500px) { 
					
				}
			
      `}</style>
    </div>
)

export default BlogCard                                                                                        
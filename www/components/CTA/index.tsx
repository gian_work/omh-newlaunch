import * as React from "react"
import Responsive from 'react-responsive'

const Desktop = props => <Responsive {...props} minWidth={768} />

export interface ctaProps {
	image?: string,
	title?: string,
  excerpt?: string,
  button?: string,
  url?: string,
}


const CTA = ({ image, title, excerpt, url, button }: ctaProps) => (
	<div className="cta-wrapper relative w-full  overflow-hidden">
    <div className="cta-container block md:flex justify-around align-center items-center">
      <div className="cta-image_wrapper mx-auto">
        <Desktop>
          <img src={image} alt={title} className="mx-auto"/>  
        </Desktop>
      </div>
      <div className="cta-content_wrapper mx-auto text-center md:text-left">
        <h3>{title}</h3>
        <p>{excerpt}</p>
        <a href={url}>{button}</a>
      </div>
    </div>
    <style jsx>{`
      .cta-wrapper {
        padding: 51px 0 55px;
      }
      .cta-content_wrapper {
        max-width: 394px;
      }
      .cta-content_wrapper h3 {
        font-weight: 600;
        font-size: 22px
        line-height: 27px;
        color: #1C1C1C;
        margin: 0 0 6px;
      }
      .cta-content_wrapper p {
        font-weight: 600;
        font-size: 24.5px
        line-height: 22px;
        color: #5F5F5F;
        margin: 0 0 24px;
      }
      .cta-content_wrapper a {
        background: #E86225;
        border-radius: 5px;
        font-weight: 600;
        font-size: 14px;
        line-height: 14px;
        padding: 15px 25px;
        color: #FFF;
        border: 1px solid #E86225;
        transition: .2s ease-in-out;
      }
      .cta-content_wrapper a:hover {
        background: #FFF;;
        color: #E86225;
      }
      @media screen and (min-width: 768px) { 
        .cta-wrapper {
          padding: 92px 0 99px;
        }
        .cta-image_wrapper {
          padding: 0 40px;
          box-sizing: border-box;
        }
        .cta-content_wrapper {
          padding: 0 40px;
          box-sizing: border-box;
        }
        .cta-content_wrapper h3 {
          font-size: 28px;
          line-height: 30px;
          margin: 0 0 12px;
        }
        .cta-content_wrapper p {
          font-size: 18px
          line-height: 21px;
          margin: 0 0 32px;
        }
      }
      @media screen and (min-width: 500px) { 

      }
    `}</style>
    </div>
)

export default CTA
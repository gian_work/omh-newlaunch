import * as React from "react"

export interface NewlaunchProps {
	featured_image?: string,
	badge?: string,
	slogan?: string,
	description?: string,
	url?: string,
}


const NewlaunchCard = ({ featured_image, badge, slogan, description, url }: NewlaunchProps) => (
	<div className="newlaunch-container block relative overflow-hidden mx-auto md:0-0 w-full md:w-1/3">
		<div className="newlaunch-image_wrapper">
			<img src={featured_image} alt={slogan}/>
			<div className="badge absolute">{badge}</div>
		</div>
		<div className="newlaunch-content_wrapper text-left">
			<h3>{slogan}</h3>
			<p>{description}</p>
			<a href={url} className="absolute inset-0"></a>
		</div>
		<style jsx>{`
				.newlaunch-container {
					max-width: 400px;
					box-sizing: border-box;
					padding: 0 0 32px;
				}
				.newlaunch-container:last-child {
					padding: 0;
				}
				.newlaunch-image_wrapper {
					margin: 0 0 14px;
				}
				.newlaunch-image_wrapper img {
					border-radius: 5px;
					overflow: hidden;
					object-fit: cover; 
					width: 400px; 
					object-position: center; 
					height: 200px;
				}
				.badge {
					background: #1C1C1C;
					opacity: 0.6;
					border-radius: 20px;
					color: #FFF;
					padding: 5px 12px;
					top: 16px;
					left: 14px;
					font-size: 13px;
					line-height: 19px;
				}
				.newlaunch-content_wrapper {

				}
				.newlaunch-content_wrapper span {
					font-weight: 600;
					font-size: 12px;
					line-height: 14px;
					margin: 0 0 4px;
				}
				.newlaunch-content_wrapper h3{
					color: #1C1C1C;
					font-weight: 600;
					font-size: 15px;
					line-height: 22px;
					margin: 0 0 7px;
				}
				.newlaunch-content_wrapper p{
					color: #5F5F5F;
					font-size: 14px;
					line-height: 20px;
					color: #5F5F5F;
				}
				@media screen and (min-width: 768px) { 
					.newlaunch-container {
						padding: 0 22px 0 0;
					}
					.newlaunch-container:nth-child(-n+3) {
						padding: 0 22px 48px 0;
					}
					.newlaunch-container:nth-child(3n) {
						padding: 0 0 48px 0;
					}
					.newlaunch-image_wrapper {
						margin: 0 0 15px;
					}
					.badge {
						font-size: 14px;
						line-height: 20px;
					}
					.newlaunch-content_wrapper span {
						font-size: 13px;
						line-height: 15px;
					}
					.newlaunch-content_wrapper h3{
						font-size: 18px;
						line-height: 21px;
						margin: 0 0 10px;
					}
					.newlaunch-content_wrapper p{
						font-size: 15px;
						line-height: 22px;
					}
				}
				@media screen and (min-width: 500px) { 
					
				}
			
      `}</style>
    </div>
)

export default NewlaunchCard                                                                                        
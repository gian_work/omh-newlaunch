import * as React from "react"
import NewlaunchCard from './index'

interface NewlaunchGroupProps {
	newlaunches?: NewLaunchProps[]
}

interface NewLaunchProps {
	featured_image?: string
	slogan?: string
	description?: string
}


const NewlaunchGroup = ({ newlaunches }: NewlaunchGroupProps) => {
  return (
    <div>
			<div className="header-tabs">
				
			</div>
			<div id="newlaunch-listings" className="blog-wrapper block md:flex flex-wrap justify-between relative">
				{
					newlaunches.map((item, index) => (
							<NewlaunchCard 
								key={index}
								featured_image = {`https://stg-api.omh.app/store/cms/media${item?.featured_image}` || 'url_fallback_image'}
								slogan = {item?.slogan || ''}
								description = {item?.description || ''}
							/>
						)
					)
				}
				<style jsx>{`
					.blog-wrapper {
					}
					.blog-image_wrapper:before {
						content: "1";
						position: absolute;
						left: 0;
						right: 0;
						bottom: 0;
						top: 0;
						text-indent: -999999px;
						overflow: hidden;
						background: linear-gradient(0.26deg, #000000 29.56%, rgba(255, 255, 255, 0) 43.52%, rgba(255, 255, 255, 0) 92.54%);
						opacity: 0.3;
					}
					@media screen and (min-width: 768px) { 
						.blog-wrapper {
						}
					}
					@media screen and (min-width: 500px) { 
						.blog-wrapper {			

						}
					}
				`}</style>
			</div>	
		</div>
  )
}

export default NewlaunchGroup
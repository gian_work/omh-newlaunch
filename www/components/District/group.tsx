import * as React from "react"
import DistrictCard, { DistrictProps } from './index'


interface DistrictGroupProps {
	District: DistrictProps[]
}

const DistrictGroup = ({ District }: DistrictGroupProps) => {

  return (
    <div className="district-wrapper flex justify-between relative w-full">
			{
				District.map((District, index) => (
						<DistrictCard 
							key={index} {...District}
							image = {District.image}
							title = {District.title}
							subtitle = {District.subtitle}
							url = {District.url}
						/>
					)
				)
			}
			<style jsx>{`
				.district-wrapper {
					overflow-x: auto;
					white-space: nowrap;
					min-width: 280px;
				}
				.district-image_wrapper:before {
					content: "1";
					position: absolute;
					left: 0;
					right: 0;
					bottom: 0;
					top: 0;
					text-indent: -999999px;
					overflow: hidden;
					background: linear-gradient(0.26deg, #000000 29.56%, rgba(255, 255, 255, 0) 43.52%, rgba(255, 255, 255, 0) 92.54%);
					opacity: 0.3;
				}
				@media screen and (min-width: 768px) { 
					.district-wrapper {
						min-width: 960px;
					}
				}
				@media screen and (min-width: 500px) { 
					.district-wrapper {			
						min-width: 340px;
					}
				}
      `}</style>
    </div>
  )
}

export default DistrictGroup
import * as React from "react"

export interface DistrictProps {
	district?: string,
	image?: string,
	url?: string,
	title?: string,
	subtitle?: string,
}


const DistrictCard = ({ image, title, subtitle, url, }: DistrictProps) => (
	<div className="district-container relative w-full  overflow-hidden">
			
			<div className="district-image_wrapper">
				<img src={image} alt={title}/>
			</div>
			<div className="district-content_wrapper absolute">
				<span className="block">{subtitle}</span>
				<h3>{title}</h3>
			</div>
			<a href={url && url || '#'} className="absolute inset-0"></a>
			<style jsx>{`
				.district-container {
					max-width: 401px;
					border-radius: 5px;
					min-width: 295px;
					min-height: 221px;
					margin-right: 22px;
					white-space: normal;
				}
				.district-container a {
					z-index: 2;
				}
				.district-image_wrapper {

				}
				.district-image_wrapper img {
					object-fit: cover;
					object-position: center;
					height: 221px;
					width: 300px;
				}
				.district-content_wrapper {
					left: 23px;
					bottom: 22px;
					transform: translate(0%,0%);
					z-index: 1;
					color: #FFF;
					max-width: 232px;
				}
				.district-content_wrapper span {
					font-weight: normal;
					font-size: 16px;
					line-height: 19px;
					color: #FFF;
					margin: 0 0 4px;
				}
				.district-content_wrapper h3 {
					font-size: 26px;
					line-height: 31px;
					color: #FFF;
				}
				@media screen and (min-width: 768px) { 
					.district-container {
						max-width: 401px;
						border-radius: 5px;
						max-height: 272px;
						min-height: 272px;
						box-sizing: border-box;
						min-width: 401px;
					}
					.district-image_wrapper img {
						height: 272px;
						width: 401px;
					}
					.district-content_wrapper span {
						font-size: 16px;
						line-height: 19px;
					}
					.district-content_wrapper h3 {
						font-size: 32px;
						line-height: 38px;
					}
				}
				@media screen and (min-width: 500px) { 
					.district-wrapper {
						min-width: 280px;
					}
				}
      `}</style>
    </div>
)

export default DistrictCard
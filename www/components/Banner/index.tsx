import * as React from "react"
import Responsive from 'react-responsive'
const Desktop = props => <Responsive {...props} minWidth={768} />
const Mobile = props => <Responsive {...props} maxWidth={767} />

export interface BannerProps {
  imageUrlDesktop?: string,
  imageUrlTablet?: string,
  imageUrlMobile?: string,
  bannerTitleDesktop?: string,
	bannerSubTitleDesktop?: string,
	bannerTitleMobile?: string,
  bannerSubTitleMobile?: string,
	bannerButton?: string,
	bannerURL?: string,
	children?: any,
	media_path?: any | 'https://api.omh.app/store/cms/media',
}


const BannerStatic = ({ imageUrlDesktop, imageUrlTablet, imageUrlMobile, bannerTitleDesktop, bannerSubTitleDesktop, bannerTitleMobile, bannerSubTitleMobile, bannerButton }: BannerProps) => (
	<div className="banner-wrapper relative w-full md:flex">
			<div className="banner-wrapper_image relative md:w-3/4">
				<div className="image">
					<figure>
						<picture>
								<source media="(min-width: 1180px)"
										srcSet={imageUrlDesktop} />
								<source media="(min-width: 768px)"
										srcSet={imageUrlTablet} />
								<img src={imageUrlMobile} alt={bannerTitleDesktop} />
							</picture>
					</figure>
				</div>
			</div>
			<div className="banner-wrapper_content relative md:text-left text-center md:w-1/4">
				<div className="content">
					<Desktop>
						<h1>{bannerTitleDesktop}</h1>
						<span className="block">{bannerSubTitleDesktop}</span>
					</Desktop>
					<Mobile>
						<h1>{bannerTitleMobile}</h1>
						<span className="block">{bannerSubTitleMobile}</span>
					</Mobile>
					
					<a className="flex md:inline-block mx-auto md:m-0 justify-center">
						{bannerButton}
						<Mobile>
							<svg width="25" height="24" viewBox="0 0 25 24" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M10.5 5.92969L9 7.42969L13.5703 12L9 16.5703L10.5 18.0703L16.5703 12L10.5 5.92969Z" fill="#E86225"/>
							</svg>
						</Mobile>
					</a>
				</div>
			</div>
			<style jsx>{`
				.banner-wrapper {
					max-width: 100%;
					max-height: 550px;
					overflow: hidden;
					display: block;
					min-height: 260px;
				}
				.banner-wrapper_image {
					max-height: 260px;
					overflow: hidden;
				}
				figure	{
					margin: 0;
				}
				img {
					top: 0;
					left: 0;
					right: 0;
					bottom: 0;
					margin: auto;
					min-width: 50%;
					min-height: 50%;
				}
				.banner-wrapper_content {
					position: relative;
					background: #F7F6F7;
					padding: 32px 22px 70px;
				}
				h1 {
					font-size: 22px;
					line-height: 26px;
					font-weight: 600;
					color: #FFF;
					margin: 0 0 14px;
					padding: 0;
					color: #1C1C1C;
				}
				span {
					font-size: 14.5px;
					line-height: 22px;
					color: #FFF;
					margin: 0 0 21px;
					color: #5F5F5F;
					font-weight: 300;
				}
				a {
					color: #E86225;
				}
				@media screen and (min-width: 1124px) { 
					.banner-wrapper_content .content {
						position: absolute;
						left: 0;
						top: 0%;
						transform: translate(0%, 50%);
						padding: 32px 70px;
					}
					.banner-wrapper_content {
						min-width: 580px;
						display: block;
						background: #f7f6f7;
						min-height: 550px;
					}
				}
				@media screen and (min-width: 768px) {
					.banner-wrapper {
						min-height: 550px;
					}
					.banner-wrapper_image {
						max-height: 10%;
					}
					.banner-wrapper_image .image {

					}
					.banner-wrapper_image .image img {
						object-fit: cover;
						object-position: center;
						min-height: 550px;
						width: 100%;
						padding: 0;
						margin: 0;
						max-height: 550px;
						box-shadow: 0 0 0 0;
					}
					.banner-wrapper_content {
						min-width: 580px;
						display: block;
						background: #f7f6f7;
						min-height: 550px;
					}
					.banner-wrapper_content .content {
						position: absolute;
						left: 0;
						top: 0%;
						transform: translate(0%, 30%);
						padding: 32px 50px;
						max-width: 500px;
					}
					h1 {
						color: #1C1C1C;
						font-size: 40px;
						line-height: 46px;
						margin: 0 0 22px;
					}
					span {
						color: #5F5F5F;
						font-size: 16px;
						line-height: 155%;
						margin: 0 0 28px;
					}
					a {
						background: #E86225;
						border-radius: 5px;
						color: #FFFFFF;
						padding: 15px 25px;
						font-weight: 500;
						font-size: 14px;
						line-height: 14px;
						transition: .2s ease-in-out;
						border: 1px solid #E86225;
					}
					a:hover {
						background: #FFF;
						color: #E86225;
					}
				}
      `}</style>
    </div>
)

export default BannerStatic
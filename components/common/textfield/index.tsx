import React from 'react'

interface iProps {
  name: string;
  type: string;
  label: string;
  onChange(event: React.FormEvent<HTMLInputElement>): void;
  onBlur(event: React.FormEvent<HTMLInputElement>): void;
  placeholder: string;
  value: string;
  hasError: any;
}

const Textfield: React.FunctionComponent<iProps> = ({
  name,
  type,
  label,
  onChange,
  onBlur,
  placeholder,
  value,
  hasError
}) => {

  function svgVisible() {
    return (
      <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path d="M12 4.5C7 4.5 2.73 7.61 1 12c1.73 4.39 6 7.5 11 7.5s9.27-3.11 11-7.5c-1.73-4.39-6-7.5-11-7.5zM12 17c-2.76 0-5-2.24-5-5s2.24-5 5-5 5 2.24 5 5-2.24 5-5 5zm0-8c-1.66 0-3 1.34-3 3s1.34 3 3 3 3-1.34 3-3-1.34-3-3-3z"></path></svg>
    )
  }

  function svgInvisible() {
    return (
      <svg focusable="false" viewBox="0 0 24 24" aria-hidden="true" role="presentation"><path fill="none" d="m0 0h24v24H0z"></path><path d="M12 7c2.76 0 5 2.24 5 5 0 .65-.13 1.26-.36 1.83l2.92 2.92c1.51-1.26 2.7-2.89 3.43-4.75-1.73-4.39-6-7.5-11-7.5-1.4 0-2.74.25-3.98.7l2.16 2.16C10.74 7.13 11.35 7 12 7zM2 4.27l2.28 2.28.46.46C3.08 8.3 1.78 10.02 1 12c1.73 4.39 6 7.5 11 7.5 1.55 0 3.03-.3 4.38-.84l.42.42L19.73 22 21 20.73 3.27 3 2 4.27zM7.53 9.8l1.55 1.55c-.05.21-.08.43-.08.65 0 1.66 1.34 3 3 3 .22 0 .44-.03.65-.08l1.55 1.55c-.67.33-1.41.53-2.2.53-2.76 0-5-2.24-5-5 0-.79.2-1.53.53-2.2zm4.31-.78l3.15 3.15.02-.16c0-1.66-1.34-3-3-3l-.17.01z"></path></svg>
    )
  }

  const [passType, setPassType] = React.useState("password")
  const currentPassType = passType === "password" ? "text" : "password"

  return (
    <div className="md-input-main">
      <div className={`md-input-box ${hasError ? 'hasError' : ''}`}>
        {type === "password"
          ? (
            <React.Fragment>
              <input
                name={name}
                type={passType}
                className="md-input font-thin"
                onChange={onChange}
                onBlur={onBlur}
                placeholder={' '}
                value={value}
              />
              <div className="md-label">{label}</div>
              <div
                className="absolute top-0 bottom-0 right-0 w-25 h-25 mr-10 my-auto"
                onClick={() => setPassType(currentPassType)}
              >
                {passType === "password" ? svgInvisible() : svgVisible()}
              </div>
            </React.Fragment>
          ) : (
            <React.Fragment>
              <input
                name={name}
                type={type}
                className="md-input font-thin"
                onChange={onChange}
                onBlur={onBlur}
                placeholder={' '}
                value={value}
              />
              <div className="md-label">{label}</div>
            </React.Fragment>
          )
        }
      </div>
      <style jsx>{`
        .md-input-box {
          position: relative;
          padding: 0 15px;
          border-radius: 4px;
          border: 1px solid #DADCDF;
          box-sizing: border-box;
        }
        .md-input-box.hasError {
          border: 1px solid #f44336;
        }
        .md-input-box:active,
        .md-input-box:focus,
        .md-input-box:focus-within {
          border: 1px solid #72C7DB;
        }
        .md-input {
          width: 100%;
          outline: none;
          height: 48px;
          padding-left: 5px;
          font-size: 1rem;
        }
        .md-label {
          background-color: #FFFFFF;
          color: #9B9B9B;
          position: absolute;
          pointer-events: none;
          transform-origin: top left;
          transform: translate(0, -36px) scale(1);
          transition: color 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms, transform 200ms cubic-bezier(0.0, 0, 0.2, 1) 0ms;
        }
        .md-input:focus + .md-label, .md-input:not(:placeholder-shown) + .md-label {
          color: #9B9B9B;
          transform: translate(0, -60px) scale(0.75);
          transform-origin: top left;
          font-size: 1rem;
          padding: 5px;
        }
      `}</style>
    </div>
  )
}

export default Textfield

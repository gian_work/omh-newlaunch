import * as React from "react"
import Responsive from 'react-responsive'
import Countdown, { zeroPad } from 'react-countdown-now'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// components
import Heading from '../../components/heading'
import Btn from '../../components/btn'
import ErrorForm from '../../components/error-form'
import Textfield from '../../../common/textfield'

// config
import OnboardService from '../../../../services/api/onboard'

function SignupOTP({ hooks }) {
  const {
    bearer,
    country,
    countryList,
    token,
    savedParams,
    setToken,
    setBearer,
    setUserData,
    pageCountry,
    screen,
    setView,
    resetOTP,
  } = hooks

  const [pin, setPin] = React.useState({
    firstDigit: '',
    secondDigit: '',
    thirdDigit: '',
    fourthDigit: ''
  })
  const [restart, setRestart] = React.useState(false)
  const [isSubmitting, setIsSubmitting] = React.useState(false)
  const [timerCount, setTimerCount] = React.useState(300)
  const [invalidOTP, setInvalidOTP] = React.useState(false)
  const [timer, setTimer] = React.useState(300000)
  const [isTimerSet, setIsTimerSet] = React.useState(false)

  const addDigit = (digit: string, value: string, next: string) => {
    setPin({
      ...pin,
      [digit]: value,
    })
    setInvalidOTP(false)
    if(value !== "" && next !== null) {
      document.getElementById(next).focus()
    }
  }

  function resendOTP() {
    return (
      OnboardService
        .updateMobile(savedParams, bearer)
        .then((response) => {
          setToken(response.data._data.token)
          setIsTimerSet(!isTimerSet)
        })
    )
  }

  function submitOTP() {
    const otp = `${pin.firstDigit}${pin.secondDigit}${pin.thirdDigit}${pin.fourthDigit}`
    setIsSubmitting(true)
    OnboardService
      .updateValidateMobile(otp, bearer)
      .then(response => {
        const { _data } = response.data

        if(_data && _data.user !== undefined) {
          setUserData(response.data._data.user)
          setBearer(response.headers["x-amzn-remapped-authorization"])
          resetOTP()
          setView("signup_success")
        } else {
          setInvalidOTP(true)
        }

        setIsSubmitting(false)
      })
  }

  function renderView() {
    if(screen.current !== 'verification_otp') {
      return null
    }

    const {
      mobile
    } = savedParams

    return (
      <div
        data-signup="signup-otp"
      >
        <div>
        <Mobile>
          <Heading
            title="OTP sent"
            subtitle="The OTP has been sent to your mobile number."
          />
        </Mobile>
        <Tablet>
          <Heading
            title="OTP sent"
            subtitle="The OTP has been sent to your mobile number."
          />
        </Tablet>
        <Desktop>
          <div
            className="text-center"
          >
            <Heading
              title="OTP sent successfully"
              subtitle="The OTP has been sent to your mobile number."
            />
          </div>
        </Desktop>
        </div>
        <div
          className="flex flex-row justify-around mb-45"
        >
          <div
            className="input-box"
          >
            <input
              id="firstDigit"
              value={pin.firstDigit}
              onChange={(e) => addDigit("firstDigit", e.target.value, "secondDigit")}
              type="tel"
              maxLength={1}
            />
          </div>
          <div
            className="input-box"
          >
            <input
              id="secondDigit"
              value={pin.secondDigit}
              onChange={(e) => addDigit("secondDigit", e.target.value, "thirdDigit")}
              type="tel"
              maxLength={1}
            />
          </div>
          <div
            className="input-box"
          >
            <input
              id="thirdDigit"
              value={pin.thirdDigit}
              onChange={(e) => addDigit("thirdDigit", e.target.value, "fourthDigit")}
              type="tel"
              maxLength={1}
            />
          </div>
          <div
            className="input-box"
          >
            <input
              id="fourthDigit"
              value={pin.fourthDigit}
              onChange={(e) => addDigit("fourthDigit", e.target.value, null)}
              type="tel"
              maxLength={1}
            />
          </div>
        </div>
        {invalidOTP && <div className="text-error text-13 text-center mb-45">{'Invalid OTP. Please try again.'}</div>}
        <div
          className="flex flex-col justify-between items-center"
        >
          <div
            className="flex text-13 pb-45"
          >
            {isTimerSet
              ?
              <Countdown
                date={Date.now() + timer}
                renderer={props => (
                  <div
                    className="text-orange"
                  >
                    {props.formatted.minutes}:{props.formatted.seconds}
                  </div>
                )}
                zeroPadTime={2}
              />
              :
              <Countdown
                date={Date.now() + timer}
                renderer={props => (
                  <div
                    className="text-orange"
                  >
                    {props.formatted.minutes}:{props.formatted.seconds}
                  </div>
                )}
                zeroPadTime={2}
              />}
            <span>&nbsp;-&nbsp;</span>
            <div
              onClick={() => resendOTP()}
            >
              Resend OTP
            </div>
          </div>

          <div
            className="flex justify-between items-center w-full"
          >
            <div
              className="text-orange text-13 cursor-pointer"
              onClick={() => setView(screen.previous)}
            >
              Back
              </div>
            <button
              type="submit"
              onClick={() => isSubmitting ? null : submitOTP()}
              className="h-btn bg-orange text-white text-15 px-25 rounded"
            >
              {isSubmitting ? "Submitting" : "Continue"}
            </button>
          </div>
        </div>
        <style jsx>{`
          [data-signup="signup-proper"] {
            display: flex;
            flex-direction: column;
            padding: 0 0 45px 0;
          }
          .input-box {
            width: 52px;
            border-bottom: 1px solid #EE620F;
          }
          .input-box > input {
            width: 100%;
            text-align: center;
            font-size: 30px;
            font-weight: 500;
            outline: none;
          }
        `}</style>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default SignupOTP
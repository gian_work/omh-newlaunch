import * as React from "react"
import { Formik } from 'formik'
import * as Yup from 'yup'
import Responsive from 'react-responsive'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// component
import Heading from '../../components/heading'
import ErrorForm from '../../components/error-form'
import MobileCountry from '../../../common/mobile-country'

// config
import OnboardService from '../../../../services/api/onboard'
import { VerifyValidation } from './validation'

function VerificationOption({ hooks }) {
  const {
    bearer,
    country,
    setCountry,
    token,
    setToken,
    social,
    countryList,
    userData,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
    setSavedParams,
  } = hooks

  const saveValue = (value: string) => {
    setFormData({ ...formData, mobile: value })
  }

  function handleSocial(type: string, params: Object) {
    if(type === 'facebook') {
      OnboardService
        .updateMobile(params, bearer)
        .then((response) => {
          setView('verification_otp', screen.current)
          setToken(response.data._data.token)
        })
    } else {
      OnboardService
        .updateMobile(params, bearer)
        .then((response) => {
          setView('verification_otp', screen.current)
          setToken(response.data._data.token)
        })
    }
  }

  function renderView() {
    if(screen.current !== "verification_option") {
      return null
    }
    return (
      <div
        className="flex flex-col"
      >
        <Mobile>
          <Heading
            title="Let's set you up"
            subtitle="Don't worry - this won't take long."
          />
        </Mobile>
        <Tablet>
          <Heading
            title="Let's set you up"
            subtitle="Don't worry - this won't take long."
          />
        </Tablet>
        <Desktop>
          <div
            className="text-center pb-20"
          >
            <Heading
              title="Let's set you up"
              subtitle="Don't worry - this won't take long."
            />
          </div>
        </Desktop>
        <div>
          <Formik
            initialValues={{
              countryCode: "",
              mobileNumber: "",
            }}
            onSubmit={(values, { setSubmitting }) => {
              const params = {
                "countryCode": `${countryList[country].code}`,
                "mobile": `${countryList[country].phone}${values.mobileNumber}`,
              }
              setSavedParams(params)
              handleSocial(social, params)
            }}
            validationSchema={VerifyValidation(
              countryList[country] === undefined ||
                countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
          >
            {props => {
              const {
                values,
                touched,
                errors,
                dirty,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
                handleReset,
              } = props
              return (
                <form onSubmit={handleSubmit}>
                  <div
                    className="mb-25"
                  >
                    <MobileCountry
                      onChange={handleChange}
                      onBlur={handleBlur}
                      countryList={countryList}
                      country={country}
                      setCountry={setCountry}
                      hasError={errors.mobileNumber && touched.mobileNumber}
                      value={values.mobileNumber}
                    />
                    {errors.mobileNumber && touched.mobileNumber && <ErrorForm>{errors.mobileNumber}</ErrorForm>}
                  </div>
                  <div
                    className="flex justify-between items-center mb-25"
                  >
                    <div
                      className="text-orange text-13 cursor-pointer"
                      onClick={() => setView("login_home")}
                    >
                      Back
                    </div>
                    <button
                      type="submit"
                      onClick={() => handleSubmit()}
                      className="h-btn bg-orange text-white text-15 px-25 rounded"
                    >
                      {isSubmitting ? "Submitting" : "Continue"}
                    </button>
                  </div>
                </form>
              )
            }}
          </Formik>
        </div>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default VerificationOption

import * as React from "react"
import axios from 'axios'

// components
import LoginHome from "./login/login-home"
import LoginProper from "./login/login-proper"
import LoginForgot from "./login/login-forgot"
import LoginPasswordSuccess from './login/login-password-success'

import SignupHome from "./signup/signup-home"
import SignupProper from "./signup/signup-proper"
import SignupOtp from "./signup/signup-otp"
import SignupExisting from "./signup/signup-existing"
import SignupProceed from "./signup/signup-proceed"
import SignupSelectRole from "./signup/signup-select-role"
import SignupShoutout from "./signup/signup-shoutout"
import SignupSuccess from "./signup/signup-success"
import SignupSocialVerify from "./signup/signup-social-verify"

import VerificationOption from "./verification/verification-option"
import VerificationOTP from "./verification/verification-otp"

// context
import { AppContext } from "../../pages/_app"

// assets
import Close from './assets/close'

function Onboard() {
  // data _app scope
  const value = React.useContext(AppContext)
  const val = value['state']
  const drawerStatus = val['drawerStatus']
  const drawerToggle = val['setDrawerStatus']
  const bearer = val['bearer']
  const setBearer = val['setBearer']
  const userData = val['userData']
  const setUserData = val['setUserData']
  const pageCountry = val['pageCountry']
  const countryList = val['countryList']
  const loginState = val['loginState']
  const setLoginState = val['setLoginState']


  // local scope
  const [step, setStep] = React.useState("login_home")
  const [token, setToken] = React.useState("")
  const [social, setSocial] = React.useState("")
  const [country, setCountry] = React.useState("0")
  const [loading, setLoading] = React.useState(false)
  const [savedParams, setSavedParams] = React.useState(false)
  const [timerCount, setTimerCount] = React.useState(10)
  const [formData, setFormData] = React.useState({
    firstName: '',
    lastName: '',
    emailAddress: '',
    mobileNumber: '',
  })

  /*
  ** steps:
    1. login_home
    2. login_proper
    3. login_forgot
    4. login_password_success
    5. signup_home
    6. signup_proper
    7. signup_otp
    8. signup_existing
    9. signup_proceed
    10. signup_selectrole
    11. signup_shoutout
    12. signup_socialverify
    13. verification_option
    14. verification_otp
  */
  const [screen, setScreen] = React.useState({
    current: 'login_home',
    previous: ''
  })
  const [otp, setOtp] = React.useState({
    first: '',
    second: '',
    third: '',
    fourth: '',
  })

  /*
  ** Control the view screen
  ** for navigating through views
  */
  function setView(current: string, previous: string) {
    setScreen({
      current: current,
      previous: previous === undefined ? "" : previous
    })
  }

  /*
  ** Reset the formData
  ** Function for reset once form submit success
  */
  function resetFormData() {
    setFormData({
      firstName: '',
      lastName: '',
      emailAddress: '',
      mobileNumber: '',
    })
  }

  /*
  ** Reset OTP
  */
  function resetOTP() {
    setOtp({
      first: '',
      second: '',
      third: '',
      fourth: '',
    })
  }

  const hooks = {
    step,
    setStep,
    country,
    setCountry,
    token,
    setToken,
    bearer,
    setBearer,
    userData,
    setUserData,
    drawerToggle,
    social,
    setSocial,
    loading,
    setLoading,
    savedParams,
    setSavedParams,
    timerCount,
    setTimerCount,
    pageCountry,
    // screen
    screen,
    setView,
    // persist form data
    formData,
    setFormData,
    // persist otp
    otp,
    setOtp,
    resetFormData,
    resetOTP,
    countryList,
    loginState,
    setLoginState,
  }

  const isLoginHome = screen.current === "login_home"

  return (
    <React.Fragment>
      <div className={`relative p-20 md:px-45 lg:px-60 ${screen.current === "signup_selectrole" ? 'lg:py-30' : 'lg:py-100'} ${screen.current === "signup_proper" ? 'lg:py-30' : 'lg:py-180'} h-full`}>
        {isLoginHome
          && (
            <div
              className="absolute top-0 left-0 p-20"
              onClick={() => setLoginState(false)}
            >
              <Close />
            </div>
          )
        }
        <LoginHome hooks={hooks} />
        <LoginProper hooks={hooks} />
        <LoginForgot hooks={hooks} />
        <LoginPasswordSuccess hooks={hooks} />

        <SignupHome hooks={hooks} />
        <SignupProper hooks={hooks} />
        <SignupOtp hooks={hooks} />
        <SignupExisting hooks={hooks} />
        <SignupProceed hooks={hooks} />
        <SignupSelectRole hooks={hooks} />
        <SignupShoutout hooks={hooks} />
        <SignupSuccess hooks={hooks} />
        <SignupSocialVerify hooks={hooks} />

        <VerificationOption hooks={hooks} />
        <VerificationOTP hooks={hooks} />
      </div>
    </React.Fragment>
  )
}

export default Onboard
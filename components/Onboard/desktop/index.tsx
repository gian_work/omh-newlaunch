import React from 'react'

// components
import Login from '../'

function OnboardDesktop({
  loginState,
  setLoginState,
}) {
  return (
    <React.Fragment>
      <div
        data-login="desktop-login"
        className={`hidden fixed top-0 bg-white z-20 lg:block xl:block ${loginState ? 'active' : ''}`}
      >
        <Login />
      </div>

      {loginState &&
        <div
          className="hidden top-0 z-10 lg:block xl:block"
          data-side="backdrop"
          onClick={() => [setLoginState(false), document.body.style.overflow = 'auto']}
        />
      }
      <style jsx>{`
        [data-login="desktop-login"] {
          width: 460px;
          height: 100vh;
          right: -100%;
          transition: all 0.3s cubic-bezier(0.25, 0.1, 0.25, 1);
        }
        [data-login="desktop-login"].active {
          right: 0;
        }
        [data-side="backdrop"] {
          background-color: rgba(0,0,0,0.3);
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          width: 100%;
          height: 100%;
          position: fixed;
        }
        [data-side="label"] {
          height: 23px;
        }

      `}</style>
    </React.Fragment>
  )
}

export default OnboardDesktop
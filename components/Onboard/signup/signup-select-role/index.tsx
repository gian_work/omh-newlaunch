import * as React from "react"
import Responsive from 'react-responsive'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// services
import OnboardService from '../../../../services/api/onboard'

// components
import Btn from '../../components/btn'
import Heading from '../../components/heading'

// assets
import iconSeller from '../../assets/icon_seller'
import iconBuyer from '../../assets/icon_buyer'
import iconLandlord from '../../assets/icon_landlord'
import iconTenant from '../../assets/icon_tenant'

const btnDetails = {
  'seller': {
    icon: iconSeller,
    title: 'Seller',
    subtitle: 'I would like to sell my property',
    value: 1
  },
  'buyer': {
    icon: iconBuyer,
    title: 'Buyer',
    subtitle: 'I am looking for a property',
    value: 2
  },
  'landlord': {
    icon: iconLandlord,
    title: 'Landlord',
    subtitle: 'I am looking to rent out my property',
    value: 3
  },
  'tenant': {
    icon: iconTenant,
    title: 'Tenant',
    subtitle: 'I would like to rent a room',
    value: 4
  }
}

function SignupSelectRole({ hooks }) {
  const {
    userData,
    bearer,
    pageCountry,
    screen,
    setView,
  } = hooks

  const [userType, setUserType] = React.useState(1)

  function userSignup() {
    const params = {
      "ethnicities": [],
      "firstName": userData.firstName || userData.givenName,
      "genderType": userData.genderType || 0,
      "languages": [],
      "lastName": userData.lastName || userData.familyName,
      "maritalStatusType": userData.maritalStatusType || 0,
      "nationalities": [],
      "userType": userType
    }

    OnboardService
      .addUserRole(params, bearer)
      .then(response => {
        setView("signup_shoutout")
      })
  }

  function renderView() {
    if(screen.current !== "signup_selectrole") {
      return null
    }
    return (
      <div
        className="flex flex-col"
      >
        <Mobile>
          <Heading
            title="Almost there! "
            subtitle="Let us know your interests"
          />
        </Mobile>
        <Tablet>
          <Heading
            title="Almost there! "
            subtitle="Let us know your interests"
          />
        </Tablet>
        <Desktop>
          <div
            className="text-center pb-20"
          >
            <Heading
              title="Almost there! "
              subtitle="Let us know your interests"
            />
          </div>
        </Desktop>

        <div>
          {Object.keys(btnDetails).map(index => (
            <div
              data-role="role-button"
              onClick={() => setUserType(btnDetails[index].value)}
              className={`flex flex-row items-center ${userType === btnDetails[index].value ? 'active' : ''}`}
            >
              {btnDetails[index].icon()}
              <div
                className="flex flex-col flex-start ml-15"
              >
                <div className="text-14 font-thin">{btnDetails[index].title}</div>
                <div className="text-12 font-thin">{btnDetails[index].subtitle}</div>
              </div>
            </div>
          ))}
        </div>
        <div
          className="flex justify-end mt-20"
        >
          <Btn
            type="default"
            label="Sign up"
            action={() => userSignup()}
          />
        </div>
        <style jsx>{`
          [data-role="role-button"] {
            width: 100% !important;
            height: 76px !important;
            padding: 0 10px 0 18px;
            border: 1px solid #DADCDF !important;
            margin-bottom: 20px !important;
            border-radius: 5px;
          }
          [data-role="role-button"].active {
            border: 2px solid #EE620F !important;
            background-color: transparent !important;
          }
        `}</style>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default SignupSelectRole
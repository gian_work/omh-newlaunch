import * as React from "react"
import Responsive from 'react-responsive'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// components
import Heading from '../../components/heading'
import Btn from '../../components/btn'

//assets
import Artwork from "../../assets/artwork"

// config
import OnboardService from '../../../../services/api/onboard'

function SignupExisting({ hooks }) {
  const {
    setToken,
    token,
    bearer,
    setBearer,
    setUserData,
    pageCountry,
    screen,
    setView,
  } = hooks

  function proceedSignup() {
    OnboardService
      .verifyConfirm(token)
      .then(response => {
        if(response.data._data.token) {
          setToken(response.data._data.token)
        }
        setUserData(response.data._data.user)
        setBearer(response.headers["x-amzn-remapped-authorization"])
        setView("signup_proceed")
      })
  }

  function renderView() {
    if(screen.current !== "signup_existing") {
      return null
    }
    return (
      <div
        className="flex flex-col items-center justify-center h-full"
      >
        <Mobile>
          <Artwork
            width={'197'}
            height={'150'}
          />
        </Mobile>
        <Tablet>
          <Artwork
            width={'197'}
            height={'150'}
          />
        </Tablet>
        <Desktop>
          <Artwork
            width={'242'}
            height={'182'}
          />
        </Desktop>

        <div
          className="text-center"
        >
          <Heading
            title="You seem familiar"
            subtitle="Looks like your mobile number has already been linked with your Google account."
          />
        </div>

        <div
          className="flex flex-col items-center justify-center"
        >
          <div
            className="mb-20  w-200"
          >
            <Btn
              type="default"
              label="Go to login"
              action={() => setView("login_home")}
            />
          </div>
          <div
            className="mb-20  w-200"
          >
            <Btn
              type="outlined"
              label="Proceed with sign up"
              action={() => proceedSignup()}
            />
          </div>
        </div>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default SignupExisting
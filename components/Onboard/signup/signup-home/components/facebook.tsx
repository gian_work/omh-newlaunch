import React from 'react'
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'

// components
import BtnSocial from '../../../components/btn-social'

// social
import Facebook from '../../../assets/facebook'

const fbAppId = process.env.OMH_APP_FACEBOOK

type IProps = {
  isFacebookActive: boolean;
  loginFacebook: Function;
  setIsFacebookActive: Function;
}

const FacebookButton: React.FC<IProps> = ({
  isFacebookActive,
  loginFacebook,
  setIsFacebookActive,
}) => {
  return (
    <React.Fragment>
      {
        isFacebookActive
          ?
          <FacebookLogin
            appId={"1847491042157912"}
            fields="name,email,picture,first_name,last_name"
            onClick={(e: Object) => loginFacebook(e)}
            callback={(e: Object) => loginFacebook(e)}
            autoLoad
            responseType="code"
            scope="public_profile,email"
            render={
              renderProps => (
                <BtnSocial
                  type="facebook"
                  label="Sign up with Facebook"
                  icon={Facebook}
                  iconPosition="left"
                  action={null}
                />
              )
            }
          />
          :
          <BtnSocial
            type="facebook"
            label="Sign up with Facebook"
            icon={Facebook}
            iconPosition="left"
            action={() => setIsFacebookActive(true)}
          />
      }
    </React.Fragment>
  )
}

export default FacebookButton
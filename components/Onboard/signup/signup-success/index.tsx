import * as React from "react"

// components
import Heading from '../../components/heading'

//assets
import Artwork from "../../assets/artwork"

function SignupSuccess({ hooks }) {
  const {
    screen
  } = hooks

  function renderView() {
    if(screen.current !== "signup_success") {
      return null
    }

    return (
      <div
        className="flex flex-col items-center"
      >
        <div>
          {Artwork}
        </div>
        <div>
          <Heading
            title={"Test Success"}
            subtitle={"This is a test view."}
          />
        </div>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default SignupSuccess
import * as React from "react"
import Responsive from 'react-responsive'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// components
import Heading from '../../components/heading'
import Btn from '../../components/btn'

//assets
import Artwork from "../../assets/artwork"

function SignupShoutout({ hooks }) {
  const {
    pageCountry,
    drawerToggle,
    screen,
    setView,
  } = hooks

  function renderView() {
    if(screen.current !== "signup_shoutout") {
      return null
    }
    return (
      <div
        className="flex flex-col justify-center items-center h-full"
      >
        <Mobile>
          <Artwork
            width={'197'}
            height={'150'}
          />
        </Mobile>
        <Tablet>
          <Artwork
            width={'197'}
            height={'150'}
          />
        </Tablet>
        <Desktop>
          <Artwork
            width={'242'}
            height={'182'}
          />
        </Desktop>

        <div
          className="text-center"
        >
          <Heading
            title="Finding room"
            subtitle="Let leasers know you’re looking for one."
          />
        </div>

        <div
          className="flex flex-col items-center justify-center"
        >
          <div
            className="mb-20 w-200"
          >
            <Btn
              type="default"
              label="Post shoutout"
              action={() => console.log('shoutout')}
            />
          </div>
          <div
            className="mb-20 w-200 text-center text-15 text-lighterGray"
          >
            <a href="#">Skip for now</a>
          </div>
        </div>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default SignupShoutout
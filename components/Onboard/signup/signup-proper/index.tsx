import * as React from "react"
import { Formik } from 'formik'
import * as Yup from 'yup'
import Responsive from 'react-responsive'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// config
import OnboardService from '../../../../services/api/onboard'
import { SignupValidation } from './validation'

// components
import Heading from '../../components/heading'
import ErrorForm from '../../components/error-form'
import Textfield from '../../../common/textfield'
import MobileCountry from '../../../common/mobile-country'

function SignupProper({ hooks }) {
  const {
    country,
    countryList,
    token,
    setToken,
    setCountry,
    setSavedParams,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
    resetFormData,
  } = hooks

  const [passwordValues, setPasswordValues] = React.useState({
    password: "",
    showPassword: false,
  })
  const [confirmPasswordValues, setConfirmPasswordValues] = React.useState({
    confirmPassword: "",
    showConfirmPassword: false,
  })

  const toggleDefaultPassword = () => {
    setPasswordValues({
      ...passwordValues,
      showPassword: !passwordValues.showPassword
    })
  }

  const toggleConfirmPassword = () => {
    setConfirmPasswordValues({
      ...confirmPasswordValues,
      showConfirmPassword: !confirmPasswordValues.showConfirmPassword
    })
  }

  const saveValue = (key, value) => {
    setFormData({ ...formData, [key]: value })
  }

  function renderView() {
    if(screen.current !== 'signup_proper') {
      return null
    }
    return (
      <div
        data-signup="signup-proper"
      >
        <Mobile>
          <Heading
            title="Let's set you up"
            subtitle="Don't worry - this won't take long."
          />
        </Mobile>
        <Tablet>
          <Heading
            title="Let's set you up"
            subtitle="Don't worry - this won't take long."
          />
        </Tablet>
        <Desktop>
          <div
            className="text-center pb-20"
          >
            <Heading
              title="Let's set you up"
              subtitle="Don't worry - this won't take long."
            />
          </div>
        </Desktop>
        <div>
          <Formik
            initialValues={{
              passwordConfirm: "",
              countryCode: "",
              mobileNumber: formData.mobileNumber || "",
              password: "",
              firstName: formData.firstName || "",
              lastName: formData.lastName || "",
              emailAddress: formData.emailAddress || "",
            }}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              const params = {
                "countryCode": `${countryList[country].code}`,
                "mobile": `+${countryList[country].phone}${values.mobileNumber}`,
                "password": values.password,
                "firstName": values.firstName,
                "lastName": values.lastName,
                "email": values.emailAddress,
              }
              setSavedParams(params)
              OnboardService
                .signup(params)
                .then((response) => {
                  setToken(response.data._data.token)
                  setView('signup_otp', screen.current)
                  resetFormData()
                  resetForm()
                });
            }}
            validationSchema={SignupValidation(
              countryList[country].code === undefined ||
                countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
          >
            {props => {
              const {
                values,
                touched,
                errors,
                dirty,
                isSubmitting,
                handleChange,
                handleBlur,
                handleSubmit,
                handleReset,
              } = props
              return (
                <form onSubmit={handleSubmit}>
                  <div
                    className="mb-25"
                  >
                    <Textfield
                      name="firstName"
                      type="text"
                      label="First Name"
                      placeholder="Placeholder"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      hasError={errors.firstName && touched.firstName}
                      value={values.firstName}
                    />
                    {errors.firstName && touched.firstName && <ErrorForm>{errors.firstName}</ErrorForm>}
                  </div>
                  <div
                    className="mb-25"
                  >
                    <Textfield
                      name="lastName"
                      type="text"
                      label="Last Name"
                      placeholder="Placeholder"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      hasError={errors.lastName && touched.lastName}
                      value={values.lastName}
                    />
                    {errors.lastName && touched.lastName && <ErrorForm>{errors.lastName}</ErrorForm>}
                  </div>
                  <div
                    className="mb-25"
                  >
                    <Textfield
                      name="emailAddress"
                      type="text"
                      label="Email Address"
                      placeholder="Placeholder"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      hasError={errors.emailAddress && touched.emailAddress}
                      value={values.emailAddress}
                    />
                    {errors.emailAddress && touched.emailAddress && <ErrorForm>{errors.emailAddress}</ErrorForm>}
                  </div>
                  <div
                    className="mb-25"
                  >
                    <MobileCountry
                      onChange={handleChange}
                      onBlur={handleBlur}
                      countryList={countryList}
                      country={country}
                      setCountry={setCountry}
                      hasError={errors.mobileNumber && touched.mobileNumber}
                      value={values.mobileNumber}
                    />
                    {errors.mobileNumber && touched.mobileNumber && <ErrorForm>{errors.mobileNumber}</ErrorForm>}
                  </div>
                  <div
                    className="mb-25"
                  >
                    <Textfield
                      name="password"
                      type="password"
                      label="Password"
                      placeholder="Placeholder"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      hasError={errors.password && touched.password}
                      value={values.password}
                    />
                    {errors.password && touched.password && <ErrorForm>{errors.password}</ErrorForm>}
                  </div>
                  <div
                    className="mb-25"
                  >
                    <Textfield
                      name="passwordConfirm"
                      type="password"
                      label="Password Confirm"
                      placeholder="Placeholder"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      hasError={errors.passwordConfirm && touched.passwordConfirm}
                      value={values.passwordConfirm}
                    />
                    {errors.passwordConfirm && touched.passwordConfirm && <ErrorForm>{errors.passwordConfirm}</ErrorForm>}
                  </div>
                  <div
                    className="flex justify-between items-center mb-25"
                  >
                    <div
                      className="text-orange text-13 cursor-pointer"
                      onClick={() => setView("signup_home", screen.current)}
                    >
                      Back
                    </div>
                    <button
                      type="submit"
                      onClick={() => handleSubmit()}
                      className="h-btn bg-orange text-white text-15 px-25 rounded"
                    >
                      {isSubmitting ? "Submitting" : "Continue"}
                    </button>
                  </div>
                </form>
              )
            }}
          </Formik>
        </div>
        <style jsx>{`
          [data-signup="signup-proper"] {
            display: flex;
            flex-direction: column;
            padding: 0 0 45px 0;
          }
        `}</style>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default SignupProper
import * as Yup from 'yup'

const Mobile = {
  'SGP': {
    min: 8,
    max: 8
  },
  'DEFAULT': {
    min: 5,
    max: 15
  }
}

export function VerifyValidation(value) {
  return (
    Yup.object().shape({
      mobileNumber: Yup
        .string()
        .min(parseInt(Mobile[value].min), 'Invalid mobile number. Please try again.')
        .max(parseInt(Mobile[value].max), 'Invalid mobile number. Please try again.')
        .required('This is required.'),
    })
  )
}
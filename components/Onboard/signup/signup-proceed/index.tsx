import * as React from "react"
import Responsive from 'react-responsive'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// components
import Heading from '../../components/heading'
import Btn from '../../components/btn'

//assets
import Artwork from "../../assets/artwork"

function SignupProceed({ hooks }) {
  const {
    pageCountry,
    screen,
    setView,
  } = hooks

  function renderView() {
    if(screen.current !== "signup_proceed") {
      return null
    }
    return (
      <div
        className="flex flex-col items-center justify-center h-full"
      >
        <Mobile>
          <Artwork
            width={'197'}
            height={'150'}
          />
        </Mobile>
        <Tablet>
          <Artwork
            width={'197'}
            height={'150'}
          />
        </Tablet>
        <Desktop>
          <Artwork
            width={'242'}
            height={'182'}
          />
        </Desktop>

        <Mobile>
          <div
            className="text-center"
          >
            <Heading
              title="Are you sure you want to proceed?"
              subtitle="You are about to create a new account."
              isCompress="mobile"
            />
          </div>
        </Mobile>

        <Tablet>
          <div
            className="text-center"
          >
            <Heading
              title="Are you sure you want to proceed?"
              subtitle="You are about to create a new account."
              isCompress="tablet"
            />
          </div>
        </Tablet>

        <Desktop>
          <div
            className="text-center"
          >
            <Heading
              title="Are you sure you want to proceed?"
              subtitle="You are about to create a new account."
              isCompress="desktop"
            />
          </div>
        </Desktop>

        <div
          className="flex flex-col items-center justify-center"
        >
          <div
            className="mb-20 w-200"
          >
            <Btn
              type="default"
              label="Continue"
              action={() => setView('signup_selectrole')}
            />
          </div>
          <div
            className="mb-20 w-200 text-center text-15 text-lighterGray"
          >
            <a href="#">Contact support</a>
          </div>
        </div>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default SignupProceed
import * as Yup from 'yup'

const Mobile = {
  'SGP': {
    min: 8,
    max: 8
  },
  'DEFAULT': {
    min: 5,
    max: 15
  }
}

export function LoginForgotValidation(value) {
  return (
    Yup.object().shape({
      mobileNumber: Yup
        .string()
        .min(parseInt(Mobile[value].min), 'Invalid mobile number. Please try again.')
        .max(parseInt(Mobile[value].max), 'Invalid mobile number. Please try again.')
        .required('This is required.'),
      emailAddress: Yup
        .string()
        .email()
        .max(100, 'Email Address too long.')
        .required('Invalid email address'),
    })
  )
}
import * as React from "react"
import { Formik } from 'formik'
import * as Yup from 'yup'
import Responsive from 'react-responsive'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// config
import OnboardService from '../../../../services/api/onboard'
import { LoginForgotValidation } from './validation'

// components
import Heading from '../../components/heading'
import ErrorForm from '../../components/error-form'
import Textfield from '../../../common/textfield'
import MobileCountry from '../../../common/mobile-country'

function LoginForgot({ hooks }) {
  const {
    country,
    setCountry,
    countryList,
    setCountryList,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
    resetFormData,
    setBearer,
    setUserData,
    setLoginState,
  } = hooks

  const [values, setValues] = React.useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  })

  const [errorMessage, setErrorMessage] = React.useState(false)
  const [password, setPassword] = React.useState('')
  const [mobile, setMobile] = React.useState('')

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  }

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  }

  const saveMobile = (value) => {
    setFormData({ ...formData, mobile: value })
  }

  const saveValue = (key, value) => {
    setFormData({ ...formData, [key]: value })
  }

  function renderView() {
    if(screen.current !== 'login_forgot') {
      return null
    }

    return (
      <div
        data-login="login-proper"
      >
        <Mobile>
          <Heading
            title="Forgot password?"
            subtitle="We'll help you get back on track."
          />
        </Mobile>
        <Tablet>
          <Heading
            title="Forgot password?"
            subtitle="We'll help you get back on track."
          />
        </Tablet>
        <Desktop>
          <div
            className="text-center pb-20"
          >
            <Heading
              title="Forgot password?"
              subtitle="We'll help you get back on track."
            />
          </div>
        </Desktop>
        <div>
          <Formik
            initialValues={{
              emailAddress: formData.emailAddress,
              mobileNumber: formData.mobileNumber,
            }}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              setErrorMessage(false)
              const params = {
                "countryCode": countryList[country].code,
                "mobile": `${countryList[country].phone}${values.mobileNumber}`,
                "email": values.emailAddress
              }
              OnboardService
                .forgotPassword(params)
                .then(response => {
                  setView("login_password_success", screen.current)
                  resetFormData()
                })
            }}
            validationSchema={LoginForgotValidation(
              countryList[country].code === undefined ||
                countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
          >
            {({
              errors,
              values,
              touched,
              isValidating,
              isSubmitting,
              handleSubmit,
              handleBlur,
              handleChange
            }) => (
                <form onSubmit={handleSubmit}>
                  <div
                    className="mb-25"
                  >
                    <Textfield
                      name="emailAddress"
                      type="text"
                      label="Email Address"
                      placeholder="Placeholder"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      hasError={errors.emailAddress === "" && touched.emailAddress}
                      value={values.emailAddress}
                    />
                    {errors.emailAddress && touched.emailAddress && <ErrorForm>{errors.emailAddress}</ErrorForm>}
                  </div>
                  <div
                    className="mb-25"
                  >
                    <MobileCountry
                      onChange={handleChange}
                      onBlur={handleBlur}
                      countryList={countryList}
                      country={country}
                      setCountry={setCountry}
                      hasError={errors.mobileNumber && touched.mobileNumber}
                      value={values.mobileNumber}
                    />
                    {errors.mobileNumber && touched.mobileNumber && <ErrorForm>{errors.mobileNumber}</ErrorForm>}
                  </div>
                  <div
                    className="flex justify-between items-center mb-25"
                  >
                    <div
                      className="text-orange text-13 cursor-pointer"
                      onClick={() => setView("login_home", screen.current)}
                    >
                      Back
                  </div>
                    <button
                      type="submit"
                      onClick={() => handleSubmit()}
                      className="h-btn bg-orange text-white text-15 px-25 rounded"
                    >
                      {isSubmitting ? "Submitting" : "Log in"}
                    </button>
                  </div>
                </form>
              )}
          </Formik>
        </div>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default LoginForgot
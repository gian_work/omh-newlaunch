import * as React from "react"

// components
import Heading from '../../components/heading'
import Btn from '../../components/btn'

// assets
import EmailSent from "../../assets/artwork-email-sent"

function LoginPasswordSuccess({ hooks }) {
  const {
    pageCountry,
    screen,
    setView
  } = hooks

  const [values, setValues] = React.useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  })

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  }

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  }

  function renderView() {
    if(screen.current !== 'login_password_success') {
      return null
    }

    return (
      <div
        className="flex flex-col justify-center h-full"
      >
        <div
          className="flex items-center justify-center"
        >
          <EmailSent />
        </div>

        <div
          className="my-22"
        >
          <div
            className="flex flex-col items-center"
          >
            <span
              className="text-24 font-semibold text-center px-40 md:px40 lg:px-80 pb-5 leading-tight"
            >
              Password reset email sent
          </span>
            <span
              className="text-16 font-light text-center"
            >
              An email has been sent to your inbox. Follow the instructions to reset your password.
          </span>
          </div>
        </div>
        <div
          className="flex justify-center"
        >
          <Btn
            type="outlined"
            label="Back to Log in"
            action={() => setView("login_proper", screen.current)}
          />
        </div>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default LoginPasswordSuccess
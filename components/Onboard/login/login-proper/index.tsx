import React from 'react'
import Responsive from 'react-responsive'
import { Formik } from 'formik'
import * as Yup from 'yup'
// import { MobileCountry } from 'omh-blocks'
import MobileCountry from '../../../common/mobile-country'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

import { LoginValidation } from './validation'

// service
import OnboardService from "../../../../services/api/onboard"
import { setUserToken } from '../../../../services/api/auth'

// component
import Heading from '../../components/heading'
import ErrorForm from '../../components/error-form'
import Textfield from '../../../common/textfield'

function LoginProper({ hooks }) {
  const {
    country,
    setCountry,
    countryList,
    setCountryList,
    setBearer,
    setUserData,
    drawerToggle,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
    resetFormData,
    setLoginState
  } = hooks

  const [errorMessage, setErrorMessage] = React.useState(false)
  const [errorType, setErrorType] = React.useState(0)
  const [password, setPassword] = React.useState('')
  const [mobile, setMobile] = React.useState('')

  function errorView(errorType) {
    if(!errorMessage) {
      return null
    }
    return (
      <div>
        {errorType === 2
          ?
          <React.Fragment>
            <div
              className="error-message text-12 leading-tight text-left lg:text-center"
            >
              <span>The Mobile number is already used in other login options.</span>
            </div>
            <style jsx>{`
            .error-message {
              padding-bottom: 20px;
              color: rgb(229, 87, 16);
            }
          `}</style>
          </React.Fragment>
          :
          <React.Fragment>
            <div
              className="error-message text-12 leading-tight text-left lg:text-center"
            >
              <span>You have entered an incorrect number, password or both. Please try again.</span>
            </div>
            <style jsx>{`
            .error-message {
              padding-bottom: 20px;
              color: red;
            }
          `}</style>
          </React.Fragment>
        }
      </div>
    )
  }

  function renderView() {
    if(screen.current !== 'login_proper') {
      return null
    }

    return (
      <div
        data-login="login-proper"
      >
        <Mobile>
          <Heading
            title="Log in to your account"
            subtitle="Please enter your log in details."
          />
        </Mobile>
        <Tablet>
          <Heading
            title="Log in to your account"
            subtitle="Please enter your log in details."
          />
        </Tablet>
        <Desktop>
          <div
            className="text-center pb-20"
          >
            <Heading
              title="Log in to your account"
              subtitle="Please enter your log in details."
            />
          </div>
        </Desktop>

        <div
          className="error-container"
        >
          {errorView(errorType)}
        </div>

        <div>
          <Formik
            initialValues={{
              mobileNumber: '',
              password: '',
            }}
            validationSchema={LoginValidation(
              countryList[country].code === undefined ||
                countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
            onSubmit={(values, { setSubmitting, resetForm }) => {
              setErrorMessage(false)
              const params = {
                "countryCode": countryList[country].code,
                "mobile": `${countryList[country].phone}${values.mobileNumber}`,
                "password": values.password
              }
              OnboardService
                .loginMobile(params)
                .then(response => {
                  if(response.data._message === "ErrorOnboardingLoginMobileFailed") {
                    setErrorType(1)
                    setSubmitting(false)
                    setErrorMessage(true)
                  } else if(response.data._message === "ErrorOnboardingLoginOtherMethodsAvailable") {
                    setErrorType(2)
                    setSubmitting(false)
                    setErrorMessage(true)
                  } 
                  else {
                    setBearer(response.headers["x-amzn-remapped-authorization"])
                    setUserData(response.data._data.user)
                    setUserToken(response.headers["x-amzn-remapped-authorization"])
                    resetForm()
                    setLoginState(false)
                    document.body.style.overflow = 'auto'
                  }
                })
            }}
          >
            {({ errors,
              values,
              touched,
              isValidating,
              isSubmitting,
              handleSubmit,
              handleBlur,
              handleChange
            }) => (
                <form onSubmit={handleSubmit}>
                  <div
                    className="mb-25"
                  >
                    <MobileCountry
                      onChange={handleChange}
                      onBlur={handleBlur}
                      countryList={countryList}
                      country={country}
                      setCountry={setCountry}
                      hasError={errors.mobileNumber && touched.mobileNumber}
                      value={values.mobileNumber}
                    />
                    {errors.mobileNumber && touched.mobileNumber && <ErrorForm>{errors.mobileNumber}</ErrorForm>}
                  </div>
                  <div
                    className="mb-25"
                  >
                    <Textfield
                      name="password"
                      type="password"
                      label="Password"
                      placeholder="Placeholder"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      hasError={errors.password && touched.password}
                      value={values.password}
                    />
                    {errors.password && touched.password && <ErrorForm>{errors.password}</ErrorForm>}
                  </div>
                  <div
                    className="text-right mb-25"
                    onClick={() => setView("login_forgot", screen.current)}
                  >
                    <span className="text-orange text-13 cursor-pointer">Forgot password?</span>
                  </div>
                  <div
                    className="flex justify-between items-center mb-25"
                  >
                    <div
                      className="text-orange text-13 cursor-pointer"
                      onClick={() => setView("login_home", screen.current)}
                    >
                      Back
                    </div>
                    <button
                      type="submit"
                      onClick={() => handleSubmit()}
                      className="h-btn bg-orange text-white text-15 px-25 rounded"
                    >
                      {isSubmitting ? "Submitting" : "Log in"}
                    </button>
                  </div>
                </form>
              )}
          </Formik>
        </div>
        <style jsx>{`
          [data-login="login-proper"] {
            display: flex;
            flex-direction: column;
            padding: 0 0 45px 0;
          }
        `}</style>
      </div >
    )
  }

  return (
    renderView()
  )
}

export default LoginProper

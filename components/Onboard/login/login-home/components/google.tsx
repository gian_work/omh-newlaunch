import React from 'react'
import GoogleLogin from 'react-google-login'

// components
import BtnSocial from '../../../components/btn-social'

// assets
import Google from '../../../assets/google'

const googleAppId = process.env.OMH_APP_GOOGLE

function GoogleButton({
  loginGoogle,
}) {
  return (
    <GoogleLogin
      clientId={"566264081176-ktt7qns23umod9tff7680pp54tib6f2b.apps.googleusercontent.com"}
      buttonText="Login"
      onSuccess={(e) => loginGoogle(e)}
      onFailure={(e) => console.error(e)}
      cookiePolicy={'single_host_origin'}
      responseType="id_token"
      render={
        renderProps => (
          <BtnSocial
            type="google"
            label="Login with Google"
            icon={Google}
            iconPosition="left"
            action={renderProps.onClick}
          />
        )
      }
    />
  )
}

export default GoogleButton
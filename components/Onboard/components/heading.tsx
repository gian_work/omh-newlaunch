import React from 'react'

interface iHeading {
  title: string;
  subtitle: string;
  isCompress?: string;
}

function Heading({
  title,
  subtitle,
  isCompress = ""
}: iHeading) {
  return (
    <div
      data-login="heading-container"
      className="flex flex-col"
    >
      <div
        className={`heading-title ${isCompress ? `compress-${isCompress}` : ''}`}
      >
        {title}
      </div>
      <div
        className="heading-subtitle"
      >
        {subtitle}
      </div>
      <style jsx>{`
        [data-login="heading-container"] {
          margin: 22px 0px;
        }
        .heading-title {
          color: rgb(33, 33, 33);
          font-size: 24px;
          font-weight: 600;
          line-height: 30px;
        }
        .heading-title.compress-mobile {
          padding: 0 20px;
          padding-bottom: 8px;
        }
        .heading-title.compress-tablet {
          padding: 0 70px;
          padding-bottom: 10px;
        }
        .heading-title.compress-desktop {
          padding: 0 60px;
          padding-bottom: 10px;
        }
        .heading-subtitle {
          font-size: 15px;
          font-weight: 300;
          color: rgb(66, 66, 66);
        }
      `}</style>
    </div>
  )
}

export default Heading

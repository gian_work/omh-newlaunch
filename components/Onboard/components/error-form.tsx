import React from 'react'

interface iErrorForm {
  children: any;
}

const ErrorForm: React.FunctionComponent<iErrorForm> = ({
  children
}) => {
  return (
    <div>
      <p data-error="text">
        {children}
      </p>
      <style jsx>{`
        [data-error="text"] {
          color: #f44336;
          margin: 8px 12px 0;
          font-size: 0.75rem;
          min-height: 1em;
          text-align: left;
          font-weight: 400;
          line-height: 1.7;
          letter-spacing: 0.03333em;
        }
      `}</style>
    </div>
  )
}

export default ErrorForm

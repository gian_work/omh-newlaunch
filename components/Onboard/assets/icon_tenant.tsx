import * as React from "react"

function iconTenant() {
  return (
    <div>
      <svg width="23" height="21" viewBox="0 0 23 21" fill="none" xmlns="http://www.w3.org/2000/svg">
        <path fillRule="evenodd" clipRule="evenodd" d="M2.32312 0V18.8752H0V21H14.8691V3.30337H18.5849V21H23V18.8752H20.6769V1.17978H14.8691V0H2.32312ZM4.41508 18.8752H12.7771V2.1236H4.41508V18.8752ZM9.29246 11.5618H11.3844V9.4382H9.29246V11.5618Z" fill="#EE620F" />
      </svg>
    </div>
  )
}

export default iconTenant
import React from 'react'

function IconFacebook() {
  return (
    <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
      <path fillRule="evenodd" clipRule="evenodd" d="M6.57288 2.8645C6.2424 2.8645 5.91192 3.26808 5.91192 3.81888V5.6916H9.18V8.40922H5.91192V16.5243H2.79072V8.40922H0V5.6916H2.79072V4.11264C2.79072 1.836 4.4064 0 6.57288 0H9.18V2.8645H6.57288Z" fill="white" />
    </svg>
  )
}

export default IconFacebook

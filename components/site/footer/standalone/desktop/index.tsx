import React from 'react'

import facebook from '../../../../../assets/svg/facebook'
import instagram from '../../../../../assets/svg/instagram'
import youtube from '../../../../../assets/svg/youtube'

function FooterStandalone() {
  return (
    <div
      className="container mx-auto md:px-25 py-25"
    >
      <div
        className="flex flex-row"
      >
        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-16 pb-5"
          >
            <a target="_blank" href="https://omh.sg/">Ohmyhome</a>
          </h2>
          <ul>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/about-us">About</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/contact-us">Contact</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/careers">Careers</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/about-us/news">Press</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/privacy-policy">Policies</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/terms-of-use">Terms of Use</a>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-16 pb-5"
          >
            <a target="_blank" href="https://omh.sg/faqs">Guides</a>
          </h2>
          <ul>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/faqs">HDB Process</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/landlords-guide">Landlords</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/buyers-guide">Buyers</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/sellers-guide">Sellers</a>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-16 pb-5"
          >
            <a target="_blank" href="https://omh.sg/blog">Blogs</a>
          </h2>
          <ul>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/blog/DIY-guides">HDB DIY</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/blog/financial-planning">Financial Planning</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/blog/integrated-housing-needs">Integrated Housing Needs</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/blog/home-rental">Home Rental</a>
            </li>
            <li
              className="text-gray text-15 pb-3"
            >
              <a target="_blank" href="https://omh.sg/blog/hot-towns">Hot Towns</a>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-lg pb-5"
          >
            Follow us
          </h2>
          <div
            className="flex items-center"
          >
            <a href="https://web.facebook.com/ohmyhomeSG/?_rdc=1&_rdr" target="_blank"><div className="pr-10">{facebook()}</div></a>
            <a href="https://www.instagram.com/ohmyhomeglobal/" target="_blank"><div className="pr-10">{instagram()}</div></a>
            <a href="https://www.youtube.com/channel/UCB67vH7GXtYeMOPGGQkDnBw" target="_blank"><div className="pr-10">{youtube()}</div></a>
          </div>
        </div>

        <div
          className="flex-1 flex flex-col justify-center"
        >
          <div
            className="flex flex-col pb-6"
          >
            <span className="text-md text-darkGray pb-1">Singapore - Ohmyhome Pte Ltd</span>
            <span className="text-sm text-gray font-light">CEA License No. L3010739Z</span>
          </div>
          <div
            className="flex flex-col"
          >
            <span className="text-md text-orange pb-1">Need help?</span>
            <span className="text-sm text-gray font-light">Singapore :  <a href="tel:+6568869009" target="_blank">+65 6886 9009</a></span>
          </div>
        </div>

      </div>
    </div>
  )
}

export default FooterStandalone

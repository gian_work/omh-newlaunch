import React from 'react'

import facebook from '../../../../../assets/svg/facebook'
import instagram from '../../../../../assets/svg/instagram'
import youtube from '../../../../../assets/svg/youtube'

function FooterDefault() {
  return (
    <div
      data-footer="container"
      className="py-6 border-b border-lighterGray mx-5"
    >
      <div>
        <ul
          className="flex"
        >
          <li
            className="font-bold text-sm text-darkGray flex flex-1 justify-center leading-none border-r	border-darkGray"
          >
            
            <a target="_blank" href="https://omh.sg/">Ohmyhome</a>
          </li>
          <li
            className="font-bold text-sm text-darkGray flex flex-1 justify-center leading-none border-r	border-darkGray"
          >
            
            <a target="_blank" href="https://omh.sg/faq">Guides</a>
          </li>
          <li
            className="font-bold text-sm text-darkGray flex flex-1 justify-center leading-none"
          >
            
            <a target="_blank" href="https://omh.sg/blog">Blogs</a>
          </li>
        </ul>
      </div>

      <div
        className="flex flex-col items-center pt-6"
      >
        <span className="text-sm text-darkGray">Singapore - Ohmyhome Pte Ltd</span>
        <span className="text-xs text-gray font-light">CEA License No. L3010739Z</span>
      </div>

      <div
        className="flex flex-col items-center pt-6"
      >
        <span className="text-sm text-darkGray">Need help?</span>
        <span className="text-xs text-gray font-light">Singapore :  +65 6886 9009</span>
      </div>

      <div
        className="flex items-center justify-center pt-6"
      >
        <a href="https://web.facebook.com/ohmyhomeSG/?_rdc=1&_rdr" target="_blank"><div className="px-2">{facebook()}</div></a>
        <a href="https://www.instagram.com/ohmyhomeglobal/" target="_blank"><div className="px-2">{instagram()}</div></a>
        <a href="https://www.youtube.com/channel/UCB67vH7GXtYeMOPGGQkDnBw" target="_blank"><div className="px-2">{youtube()}</div></a>
      </div>
    </div>
  )
}

export default FooterDefault

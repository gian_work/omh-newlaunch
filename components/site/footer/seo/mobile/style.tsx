import React from 'react'

function Style() {
  return (
    <style jsx>{`
      .transition {
        transition: all 0.5s ease-in-out;
      }

      .flipIn {
        animation: flipdown 0.5s ease both;
      }

      [data-footer="container"] {
        padding: 20px 0;
      }

      [data-footer="main-list"] {
        padding: 0 28px;
      }

      [data-tabs="footer-link-tab"] {
        position: relative;
        margin: 0;
        padding-bottom: 15px;
        animation: flipdown 0.5s ease both;
      }

      [data-tabs="footer-link-tab"]&:nth-of-type(1) {
        animation-delay: 0.5s;
      }

      [data-tabs="footer-link-tab"]&:nth-of-type(1) {
        animation-delay: 0.75s;
      }

      [data-tabs="footer-link-tab"]&:nth-of-type(1) {
        animation-delay: 1s;
      }

      [data-tabs="footer-link-tab"]&:last-of-type {
        padding-bottom: 0;
      }

      [data-list="footer-seo-links"] {
        position: relative;
        overflow: hidden;
        max-height: 800px;
        opacity: 1;
        transform: translate( 0 , 0 )  ;
        margin-top: 14px;
        z-index: 2;
        margin-top: 0;
        max-height: 0;
        opacity: 0;
        transition: all 0.5s ease-in-out;
      }

      input[type=checkbox] {
        position: absolute;
        cursor: pointer;
        width: 100%;
        height: 100%;
        z-index: 1;
        opacity: 0;
      }

      i {
        position: absolute;
        transform: translate( -6px , 0 );
        margin-top: 8px;
        right: 0;
      }
      
      i:before , i:after {
        content: "";
        position: absolute;
        background-color: gray;
        width: 2px;
        height: 8px;
        transition: all 0.5s ease-in-out;
      }
        
      i:before {
        transform: translate( -2px , 0 ) rotate( -45deg );
      }
          
      i:after {
        transform: translate( 2px , 0 ) rotate( 45deg );
      }

      input[type=checkbox]:checked ~ [data-list="footer-seo-links"] {
        margin-top: 0;
        max-height: 800px;
        opacity: 1;
        transform: translate( 0 , 10% );
      }

      input[type=checkbox]:checked ~ i:before {
        transform: translate( -2px , 0 ) rotate( 45deg );
      }
            
      input[type=checkbox]:checked ~ i:after {    
        transform: translate( 2px , 0 ) rotate( -45deg );
      }

      @keyframes flipdown {
        0% {
          opacity: 0;
          transform-origin: top center;
          transform: rotateX(-90deg);
        }
        5% {
          opacity: 1;
        }
        80% {
          transform: rotateX(8deg);
        }
        83% {
          transform: rotateX(6deg);
        }
        92% {
          transform: rotateX(-3deg);
        }
        100% {
          transform-origin: top center;
          transform: rotateX(0deg);
        }
      }
    `}</style>
  )
}

export default Style
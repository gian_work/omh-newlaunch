import React from 'react'
import Link from 'next/link'

import Style from './style'

const data = {
  "buy_homes": {
    "name": "Buy homes",
    "links": [
      {
        "name": "test link",
        "href": "#"
      },
      {
        "name": "test link",
        "href": "#"
      },
      {
        "name": "test link",
        "href": "#"
      }
    ]
  },
  "rent_home": {
    "name": "Rent home",
    "links": [
      {
        "name": "test link",
        "href": "#"
      },
      {
        "name": "test link",
        "href": "#"
      },
      {
        "name": "test link",
        "href": "#"
      }
    ]
  },
  "list_home": {
    "name": "List home",
    "links": [
      {
        "name": "test link",
        "href": "#"
      },
      {
        "name": "test link",
        "href": "#"
      },
      {
        "name": "test link",
        "href": "#"
      }
    ]
  },
  "services": {
    "name": "Services",
    "links": [
      {
        "name": "test link",
        "href": "#"
      },
      {
        "name": "test link",
        "href": "#"
      },
      {
        "name": "test link",
        "href": "#"
      }
    ]
  }
}

function FooterSeo() {
  return (
    <div
      data-footer="container"
    >
      <ul
        data-footer="main-list"
      >
        {Object
          .keys(data)
          .map((item, index) => (
            <li
              data-tabs="footer-link-tab"
            >
              <input
                type="checkbox"
              />
              <i />
              <h2
                className="text-darkGray text-sm"
              >
                {data[item].name}
              </h2>
              <ul
                data-list="footer-seo-links"
              >
                {data[item].links.map(item => (
                  <li
                    className="text-darkGray text-sm"
                  >
                    <Link
                      href={`${item.href}`}
                    >
                      {item.name}
                    </Link>
                  </li>
                ))}
              </ul>
            </li>
          ))}
      </ul>
      {Style()}
    </div>
  )
}

export default FooterSeo

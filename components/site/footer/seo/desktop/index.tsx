import React from 'react'
import Link from 'next/link'

import Style from './style'

function FooterSeo() {
  return (
    <div
      className="container mx-auto md:px-25 py-25"
    >
      <div
        className="flex"
      >
        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-13 pb-5"
          >
            Top Categories
          </h2>
          <ul>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">HDB for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Condo for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Landed for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Yishun</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Sengkang</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Punggol</Link>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-13 pb-5"
          >
            Top Categories
          </h2>
          <ul>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">HDB for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Condo for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Landed for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Yishun</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Sengkang</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Punggol</Link>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-13 pb-5"
          >
            Top Categories
          </h2>
          <ul>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">HDB for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Condo for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Landed for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Yishun</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Sengkang</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Punggol</Link>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-13 pb-5"
          >
            Top Categories
          </h2>
          <ul>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">HDB for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Condo for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Landed for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Yishun</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Sengkang</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Punggol</Link>
            </li>
          </ul>
        </div>

        <div
          className="flex-1"
        >
          <h2
            className="text-darkGray text-13 pb-5"
          >
            Top Categories
          </h2>
          <ul>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">HDB for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Condo for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Landed for Sale</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Yishun</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Sengkang</Link>
            </li>
            <li
              className="text-gray text-13 pb-3"
            >
              <Link href="#">Homes in Punggol</Link>
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}

export default FooterSeo

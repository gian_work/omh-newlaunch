/*
 * Created on Thu Sep 05 2019
 *
 * Footer Component
 * type ['seo', 'default']
 */

import React from 'react'
import dynamic from 'next/dynamic'

const FooterSeo = dynamic(import('./seo'))
const FooterDefault = dynamic(import('./default'))
const FooterStandalone = dynamic(import('./standalone'))

export default function Footer(Component: any) {
  let Footer: any

  switch(Component.type) {
    case 'seo':
      Footer = FooterSeo
      break
    case 'standalone':
      Footer = FooterStandalone
      break
    default:
      Footer = FooterDefault
  }

  return (
    <Footer>
      {Component.children}
    </Footer>
  )
}
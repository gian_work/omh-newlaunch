import React from 'react'

import ChevronDown from '../../../../assets/svg/chevron-down'

function loggedIn({
  userData,
  handleLogout
}) {
  return (
    <div>
      <div data-header="user-block">
        <div className="flex items-center cursor-pointer text-sm text-blue border border-white border-b-0 group-hover:border-grey-light rounded-t-lg"
        >
          <div
            className="rounded-full h-40 w-40 flex items-center justify-center overflow-hidden"
          >
            <div
              style={{
                backgroundImage: `url(${userData.profileUrl ? userData.profileUrl : '/static/sample_avatar.jpg'})`,
                backgroundSize: 'cover',
                width: '100%',
                height: '100%',
              }}
            />
          </div>
          <span
            data-user="firstname"
            className="pl-4 text-15 text-darkGray"
          >
            {userData.firstName}
          </span>
          <div
            className="pl-10"
          >
            <ChevronDown fill="#A8A8A8" />
          </div>
        </div>

        <div data-header="user-block-options" className="shadow bg-white overflow-hidden absolute w-56 hidden">
          <a
            href="#"
            className="no-underline hover:bg-mediumGray block px-4 py-10 text-15 text-darkGray"
          >
            Action
          </a>
          <a
            href="#"
            className="no-underline hover:bg-mediumGray block px-4 py-10 text-15 text-darkGray bg-white"
          >
            Another action
          </a>
          <a
            href="/sg/profile"
            className="no-underline hover:bg-mediumGray block px-4 py-10 text-15 text-darkGray bg-white"
          >
            Profile
          </a>
          <a
            className="no-underline hover:bg-mediumGray block px-4 py-10 text-15 text-darkGray bg-white cursor-pointer"
            onClick={() => handleLogout()}
          >
            Logout
          </a>
        </div>

      </div>
      <style jsx>{`
        [data-header="user-block"]:hover > [data-header="user-block-options"] {
          display: block !important;
        }
        [data-user="firstname"] {
          white-space: nowrap;
          min-width: 50px;
          text-overflow: ellipsis;
          overflow: hidden;
          max-width: 100px;
        }
      `}</style>
    </div>
  )
}

export default loggedIn

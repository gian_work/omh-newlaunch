import React from 'react'
import Link from 'next/link'

function guest({
  setLoginState
}) {
  return (
    <div>
      <ul
        className="flex"
      >
        <li>
          <div
            onClick={() => setLoginState(true)}
          >
            <span className="text-orange text-14 pr-15 cursor-pointer">Sign up</span>
          </div>
        </li>
        <li>
          <div
            onClick={() => [setLoginState(true), document.body.style.overflow = 'hidden']}
          >
            <span className="text-14 text-darkGray cursor-pointer">Login</span>
          </div>
        </li>
      </ul>
    </div>
  )
}

export default guest

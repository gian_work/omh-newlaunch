import React from 'react'
import Link from 'next/link'

import ChevronDown from '../../../../../assets/svg/chevron-down'
import Profile from '../../../../../assets/svg/menu/profile'

function LoggedIn({
  userData,
  handleLogout
}) {
  return (
    <React.Fragment>
      <div className="flex h-full items-center justify-between">
        <div className="relative flex mx-15 w-full h-full">
          <input
            className="h-50"
            data-side="input-checkbox"
            type="checkbox"
          />
          <div
            className="rounded-full h-50 w-50 h-50 flex overflow-hidden mr-15"
          >
            <img src="/static/sample_avatar.jpg" />
          </div>
          <div data-side="user-block-content" className="flex-1">
            <div className="pt-3">
              <div className="flex items-center justify-between">
                <div className="text-white text-15 font-semibold block">{userData.displayName}</div>
                <div className="arrow"><ChevronDown fill="#FFFFFF" /></div>
              </div>
              <span className="text-white text-13 font-thin block">{userData.email}</span>
            </div>
            <ul data-side="user-block-links" className="border-t border-transparentWhite">
              <li>
                {/* <Link
                  href="/sg/profile"
                  className="flex items-center text-white text-14 font-thin"
                >
                  Profile
                </Link> */}
                <a
                  href="/sg/profile"
                  className="flex items-center text-white text-14 font-thin"
                >
                  <span className="pr-10"><Profile /></span>
                  <span>Profile</span>
                </a>
              </li>
              <li>
                <a
                  className="flex items-center text-white text-14 font-thin cursor-pointer"
                  onClick={() => handleLogout()}
                >
                  <span className="pr-10"><Profile /></span>
                  <span>Log out</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
        <style jsx>{`
            [data-side="user-block"] {
              background: linear-gradient(90deg, rgba(238,98,15,1) 0%, rgba(249,114,35,1) 100%);
            }
            [data-side="user-block-checkbox"] {
              position: absolute;
              cursor: pointer;
              width: 100%;
              height: 40px;
              z-index: 1;
              opacity: 0;
            }
            [data-side="user-block-content"] > [data-side="user-block-links"] {
              position: relative;
              opacity: 1;
              z-index: 2;
              max-height: 0;
              opacity: 0;
            }
            [data-side="input-checkbox"] {
              position: absolute;
              cursor: pointer;
              width: 100%;
              height: 40px;
              z-index: 1;
              opacity: 0;
            }
            input[type=checkbox]:checked ~ [data-side="user-block-content"] > [data-side="user-block-links"] {
              max-height: 800px;
              opacity: 1;
              padding: 10px 0;
              margin-top: 10px;
            }
            input[type=checkbox]:checked ~ [data-side="user-block-content"] > [data-side="user-block-links"] > li {
              padding: 5px 0;
            }
            input[type=checkbox]:checked ~ [data-side="user-block-content"] .arrow {
              transform: rotate(180deg);
            }
          `}</style>
      </div>
    </React.Fragment>
  )
}

export default LoggedIn
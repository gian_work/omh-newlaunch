import React from 'react'

import LoggedIn from './logged-in'
import LoggedOut from './logged-out'

import { AppContext } from '../../../../../pages/_app'

function UserBlock({
  setLoginState,
  handleLogout
}) {

  const Global = React.useContext(AppContext)
  const userData = Global['state']['userData']

  const isLoggedIn = Object.keys(userData).length !== 0

  return (
    <div data-side="user-block" className="py-45">
      {isLoggedIn
        ? <LoggedIn userData={userData} handleLogout={handleLogout} />
        : <LoggedOut setLoginState={setLoginState} />
      }
      <style jsx>{`
        [data-side="user-block"] {
          background: linear-gradient(90deg, rgba(238,98,15,1) 0%, rgba(249,114,35,1) 100%);
        }
      `}</style>
    </div>
  )
}

export default UserBlock
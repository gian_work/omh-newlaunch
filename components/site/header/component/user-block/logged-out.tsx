import React from 'react'

function LoggedOut({
  setLoginState
}) {
  return (
    <div
      className="flex h-full items-center justify-between"
      onClick={() => setLoginState(true)}
    >
      <div className="relative flex items-center mx-15 w-full h-full">
        <div
          className="rounded-full h-50 w-50 h-50 flex overflow-hidden mr-15"
        >
          <img src="/static/sample_avatar.jpg" />
        </div>
        <div className="text-white text-15 font-semibold">Start by signing in</div>
      </div>
    </div>
  )
}

export default LoggedOut

import React from 'react'
import Responsive from 'react-responsive'
import Cookies from 'js-cookie'

import {logout} from '../../../services/api/auth'

import MobileContent from './mobile'
import DesktopContent from './desktop'

const Mobile = (props: any) => <Responsive {...props} maxWidth={767} />
const Tablet = (props: any) => <Responsive {...props} minWidth={768} maxWidth={991} />
const Desktop = (props: any) => <Responsive {...props} minWidth={992} />

// Context
import { AppContext } from '../../../pages/_app'

export default function PageHeader(props) {

  const Global = React.useContext(AppContext)
  let loginState = Global['state']['loginState']
  let setLoginState = Global['state']['setLoginState']
  let sideMenuState = Global['state']['sideMenuState']
  let setSideMenuState = Global['state']['setSideMenuState']
  let userData = Global['state']['userData']
  let setUserData = Global['state']['setUserData']

  const headerProps = {
    loginState,
    setLoginState,
    sideMenuState,
    setSideMenuState,
    userData,
    setUserData,
  }

  function handleLogout() {
    logout()
    window.location.reload()
  }

  const { 
    standalone,
    links
  } = props

  return (
    <div
      className="header-main"
    >
      <Mobile>
        <MobileContent
          headerProps={headerProps}
          handleLogout={handleLogout}
          standalone={standalone}
          links={links}
        />
      </Mobile>

      <Tablet>
        <MobileContent
          headerProps={headerProps}
          handleLogout={handleLogout}
          standalone={standalone}
          links={links}
        />
      </Tablet>

      <Desktop>
        <DesktopContent
          headerProps={headerProps}
          handleLogout={handleLogout}
          standalone={standalone}
          links={links}
        />
      </Desktop>
      <style jsx>{`
        .header-main {
          width: 100%;
          position: fixed;
          z-index: 10;
          background-color: #FFFFFF;
          border-bottom: 1px solid #E8E8E8;
        }
      `}</style>
    </div>
  )
}

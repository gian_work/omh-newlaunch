import React from 'react'
import Link from 'next/link'

// components
import UserBlock from '../component/user-block'
import Onboard from '../../../Onboard'

// assets
import Phone from '../../../../assets/svg/phone'
import Menu from '../../../../assets/svg/menu-burger'
import Logo from '../../../../assets/svg/logo'
import ChevronDown from '../../../../assets/svg/chevron-down'
import Magnify from '../../../../assets/svg/menu/magnify'
import Icons from '../../../../assets/svg/menu/icons'
import Profile from '../../../../assets/svg/menu/profile'

const orange = '#EE620F'
const white = '#FFFFFF'

function Header({
  headerProps,
  handleLogout,
  standalone,
  links
}) {
  const {
    loginState,
    setLoginState,
    sideMenuState,
    setSideMenuState,
    userData,
  } = headerProps

  return (
    <div>
      <div
        className="flex justify-between items-center h-headerSm px-15"
      >
        <div
          onClick={() => [setSideMenuState(true), document.body.style.overflow = 'hidden']}
        >
          {Menu(orange, 'mobile')}
        </div>
        <div>
          <a href={'https://omh.sg/'}>
            {Logo(orange)}
          </a>
        </div>
        <div>
          <a href={'tel:65 6886 9009'}>
            {Phone(orange)}
          </a>
        </div>
      </div>

      <div
        className={`mobile-side-nav md:w-460 absolute bg-white shadow inset-y-0 bottom-0 z-30 h-screen ${sideMenuState ? 'active' : ''}`}
      >
        {
          loginState
          ? (
            <React.Fragment>
              <Onboard />
            </React.Fragment>
          ) : (
            standalone ?
              <React.Fragment>
                <a href={'https://omh.sg/'} target='_blank'>
                  <UserBlock
                    setLoginState={() => {}}
                    handleLogout={() => {}}
                  />
                </a>
                <div className="relative pt-25 pb-20">
                  {
                    links && links.map(link => {
                    return(
                      <a
                        className="flex items-center justify-between h-full mt-10"
                        href={link.url}
                        
                      >
                        <div className="flex items-center h-40 w-full">
                          <div className="ml-25 mr-15">{Icons(link.icon)}</div>
                          <div className="text-15" data-side="label">{link.text}</div>
                        </div>
                      </a>
                    )
                  })
                }
                </div>
              </React.Fragment>
            : 
              <React.Fragment>
                <div>
                  <UserBlock
                    setLoginState={setLoginState}
                    handleLogout={handleLogout}
                  />
                </div>
                <div className="relative pt-25 pb-20">
                <input
                  data-side="input-checkbox"
                  type="checkbox"
                />
                <div
                  className="flex items-center justify-between h-full"
                >
                  <div className="flex items-center h-40">
                    <div className="ml-25 mr-15">{Magnify('#343434')}</div>
                    <div className="text-15" data-side="label">Buy</div>
                  </div>
                  <div className="border-l-2 border-mediumDarkGray flex items-center px-20" data-side="label">
                    <div className="arrow"><ChevronDown fill="#484848" /></div>
                  </div>
                </div>
                
              </div>
              </React.Fragment>
          )
        }

      </div>

      {sideMenuState &&
        <div
          data-side="backdrop"
          onClick={() => [setSideMenuState(false), document.body.style.overflow = 'auto']}
        />
      }

      <style jsx>{`
        .mobile-side-nav {
          width: 316px;
          left: -100%;
          transition: all 0.3s cubic-bezier(0.25, 0.1, 0.25, 1);
        }
        .mobile-side-nav.active {
          left: 0;
        }
        [data-side="backdrop"] {
          background-color: rgba(0,0,0,0.3);
          top: 0;
          left: 0;
          right: 0;
          bottom: 0;
          width: 100%;
          height: 100vh;
          position: absolute;
        }
        [data-side="label"] {
          height: 23px;
        }
        [data-side="input-checkbox"] {
          position: absolute;
          cursor: pointer;
          width: 100%;
          height: 40px;
          z-index: 1;
          opacity: 0;
        }
        [data-side="links"] {
          position: relative;
          overflow: hidden;
          margin-top: -15px ;
          max-height: 800px;
          opacity: 1;
          transform: translate( 0 , 0 )  ;
          z-index: 2;
          max-height: 0;
          opacity: 0;
          transition: all 0.5s ease-in-out;
        }
        input[type=checkbox]:checked ~ [data-side="links"] {
          max-height: 800px;
          opacity: 1;
          transform: translate( 0 , 10% );
        }
        input[type=checkbox]:checked ~ div > [data-side="label"] > .arrow {
          transform: rotate(180deg);
        }
      `}</style>
    </div>
  )
}

export default Header
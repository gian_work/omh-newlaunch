import Document, { Html, Head, Main, NextScript } from 'next/document';
import flush from 'styled-jsx/server';

class MyDocument extends Document {
  static async getInitialProps(ctx) {
    // console.log(ctx)
    const initialProps = await Document.getInitialProps(ctx)
    // initialProps.styles = flush()
    return { ...initialProps, ...ctx}
  }

  render() {
    return (
      <Html>
        <Head />
        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    );
  }
}


export default MyDocument;
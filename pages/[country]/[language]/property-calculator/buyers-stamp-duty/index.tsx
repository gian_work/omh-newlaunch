import React from 'react'
import MainCalculator from 'omh-calculators'
import axios from 'axios'
import { useRouter } from 'next/router'
import * as TagManager from 'react-gtm-module'
import PageLayout from '../../../../../layouts'
import Head from '../../../../../components/site/page-head/dynamic'

import '../../../../../styles/tailwind.css'
import { AppContext } from ".././../../../_app"

const COUNTRY = {
  'sg': 'SGP',
  'my': 'MYS',
}

const BuyersStampDuty = () => {
  const value = React.useContext(AppContext)
  const val = value['state']
  // const countryList = val['countryList']
  const env = val['environment']
  const router = useRouter()
  const { language = 'en', country = 'sg' } = router && router.query || {}
  const cty = country && country.toString() 
  const lang = language && language.toString() 
  let url = 'https://ohmyhome.com/sg/en/property-calculator/buyers-stamp-duty'
  if (typeof window !== 'undefined' && router.pathname === "/[country]/[language]/property-calculator/buyers-stamp-duty") { 
    url = window.location.href
    const tagManagerArgs = {
      dataLayer: {
        countryCode: `${COUNTRY[cty]}`,
        environmentCode: `${COUNTRY[cty]}-${env}`,
        pageName: 'buyer_stamp_duty'
      }
    }
    TagManager.dataLayer(tagManagerArgs)
  }

  return (
    <React.Fragment>
      <Head
        title="Ohmyhome | Buyer's Stamp Duty Calculator"
        description={"To calculate buyer's stamp duty"}
        url={url}
        ogTitle={"Ohmyhome | Financial Calculators | Property Taxes | Plan your finances when Buying a Property | Buyer's Stamp Duty Calculator | Financial Tools for Home Buyers"}
        ogDesc={"What are the costs and fees that you need to take into account when you buy a property. Calculate your property taxes with buyer's stamp duty (BSD) calculator powered by IRAS."}
        ogTags={"Ohmyhome Financial Calculators, financial planning tools, buy property, stamp duty calculators, property taxes, buyer's stamp duty, Property calculators, BSD, ABSD"}
        ogImage={"https://z.omh.sg/media/singapore/Calculator_Meta_image.png"}
      />
      <div className={'main__content'}>
        <MainCalculator
          calcuType={'buyer-stamp-duty'}
          lang={lang && lang.toString().toUpperCase() || 'EN'}
        />
      </div>
      <style jsx>{`
          .main__content{
            margin-top: 56px;
          }
          @media (min-width : 1224px){
            .main__content{
              margin-top: 70px;
            }
          }
        `}
        </style>
    </React.Fragment>
  )
}

export default PageLayout(BuyersStampDuty, 'standalone')

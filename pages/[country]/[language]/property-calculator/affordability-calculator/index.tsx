import React from 'react'
import MainCalculator from 'omh-calculators'
import { useRouter } from 'next/router'
import * as TagManager from 'react-gtm-module'
import PageLayout from '../../../../../layouts'
import Head from '../../../../../components/site/page-head/dynamic'

import '../../../../../styles/tailwind.css'
import { AppContext } from ".././../../../_app"

const COUNTRY = {
  'sg': 'SGP',
  'my': 'MYS',
}

const AffordabilityCalculator = () => {
  const value = React.useContext(AppContext)
  const val = value['state']
  // const countryList = val['countryList']
  const env = val['environment']
  const router = useRouter()
  const { language = 'en', country = 'sg' } = router && router.query || {}
  const lang = language && language.toString() 
  const cty = country && country.toString() 
  let url = 'https://ohmyhome.com/sg/en/property-calculator/affordability-calculator'
  if (typeof window !== 'undefined' && router.pathname === "/[country]/[language]/property-calculator/affordability-calculator") {
    url = window.location.href
    const tagManagerArgs = {
      dataLayer: {
        countryCode: `${COUNTRY[cty]}`,
        environmentCode: `${COUNTRY[cty]}-${env}`,
        pageName: 'affordability_calculator_page'
      }
    }
    TagManager.dataLayer(tagManagerArgs)
  }
  return (
    <React.Fragment>
      <Head
        title="Ohmyhome | Property Affordability Calculator"
        description={"To calculate buying property affordability"}
        url={url}
        ogTitle={'Ohmyhome | Financial Calculations | Plan your Finances when Buying or Selling your Property | Affordability Calculator | Home Seller Financial Tools | Home Buyer Financial Tools'}
        ogDesc={'Property purchase requires the right financial planning tools which include the amount of cash required to buy, CPF savings, CPF Housing Grants, and the housing loan you can secure. Find out whether the property is within your budget.'}
        ogTags={"Ohmyhome Financial Calculators, financial planning tools, buy property, sales proceeds calculator, affordability calculator, monthly repayment calculator, home loan calculator, mortgage calculator, stamp duty calculators, buyer's stamp duty, seller's stamp duty, tenant's stamp duty"}
        ogImage={"https://z.omh.sg/media/singapore/Calculator_Meta_image.png"}
      />
        <div className={'main__content'}>
          <MainCalculator
            calcuType={'affordability'}
            lang={lang && lang.toString().toUpperCase() || 'EN'}
          />
        </div>
        <style jsx>{`
          .main__content{
            margin-top: 56px;
          }
          @media (min-width : 1224px){
            .main__content{
              margin-top: 70px;
            }
          }
        `}
        </style>
    </React.Fragment>
  )
}

export default PageLayout(AffordabilityCalculator, 'standalone')

import React from 'react'
import ProjectDetails from 'omh-project-details'
import PageLayout from '../../../../../layouts'
import { useRouter } from 'next/router'
import * as TagManager from 'react-gtm-module'
import '~/styles/tailwind.css'
import Head from '../../../../../components/site/page-head/dynamic'
import { AppContext } from ".././../../../_app"

const COUNTRY = {
  'sg': 'SGP',
  'my': 'MYS',
}

const NewLaunches = () => {
  const router = useRouter()
  const { language = 'en', country = 'sg', slug = '',  } = router && router.query || {}


  const value = React.useContext(AppContext)
  const val = value['state']
  const env = val['environment']
  const lang = language && language.toString() 
  const cty = country && country.toString() 
  let url = ''
  if (
      typeof window !== 'undefined' 
      && router.pathname === "/[country]/[language]/new-property-launch/[slug]"
      && slug != ''
    ) {
    url = window.location.href
    const tagManagerArgs = {
      dataLayer: {
        countryCode: `${COUNTRY[cty]}`,
        environmentCode: `${COUNTRY[cty]}-${env}`,
        pageName: 'project_details_page',
        project_name: slug || 'project-name'
      }
    }
    TagManager.dataLayer(tagManagerArgs)
  }
  
  return(
    <div>
      <Head
        title="Ohmyhome | New Launches"
        description={""}
        url={url}
        ogTitle={'Ohmyhome | New Launches'}
        ogDesc={''}
        ogTags={""}
        ogImage={""}
      />
      {
        slug &&
          <ProjectDetails
            id={2}
            slug={slug || "" }
            country={country || ""}
            language={language || ""}
          />
      }
    </div>
  )
}

export default PageLayout(NewLaunches, 'standalone')
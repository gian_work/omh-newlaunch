import React from 'react'
import axios from 'axios'
import PageLayout from '../../../../layouts'

import '../../../../styles/tailwind.css'

const OmhSg = () => {
return (
  <div className={'mt-70'}>
    <div className={'mt-80'}>
      Home
    </div>
    <style jsx>{`
      
      div.listCard {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        max-width: 1280px;
        margin: 0px auto;
        margin-top: 50px;
      }
    `}</style>
  </div>
)}

export default PageLayout(OmhSg, 'default')

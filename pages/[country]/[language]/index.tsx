import React from 'react'
import axios from 'axios'
import Link from 'next/link'
import { useRouter } from 'next/router'
import PageLayout from '../../../layouts'
import { Cards, SlickSlider } from 'omh-blocks'

import '../../../styles/tailwind.css'

const OmhSg = () => {

  const [featuredList, setFeaturedList] = React.useState([])
  const [latestList, setLatestList] = React.useState([])
  const router = useRouter()

  React.useEffect(() => {
    // async function fetchFeatured() {
    //   const response = await axios('https://qa.omh.sg/api/home/featured-listings')
    //   const { data } = response
    //   data && setFeaturedList(data.data.list)
    // }
    // fetchFeatured()
    // async function fetchLatest() {
    //   const response = await axios('https://qa.omh.sg/api/home/latest-listings')
    //   const { data } = response
    //   data && setLatestList(data.data.list)
    // }
    // fetchLatest()
  
  }, [])

return (
  <div>
      <div className="hero">
      {/* <img src="https://misdirection.ohmyhome.com/cms/media/malaysia/home_banner.jpg" /> */}
      <div className={'listCard'}>
        {
          latestList && latestList.length ?
            latestList.slice(0,8).map((list, key) => {
              const { obj } = list
              const {
                _id,
                amount,
                address,
                attachments,
                numberOfBedrooms
              } = obj

              const imageUrls = attachments && attachments.map( a => a.url)
              return(
                <Cards
                  imageUrls={imageUrls || ['https://omh.sg/themes/omh/assets/dist/img/no_image_listing_placeholder.png']}
                  propertyFor={'For Sale'}
                  propertyType={'Condo'}
                  propertyRoom={`3 Rooms`}
                  propertyAmount={amount}
                  propertyPerSquare={'1449'}
                  propertyAddress={'Blk 556 Toa Payoh Street 21 Jackson'}
                  countBed={numberOfBedrooms}
                  countToilet={'3'}
                  countArea={'3'}
                  countParking={'3'}
                  id={_id}
                  key={key}
                  isFave={true}
                  favoriteCallback={(id) => {
                  }}
                />
              )
            })
            : ''
        }
      </div>
    </div>

    <style jsx>{`
      div.listCard {
        display: flex;
        flex-wrap: wrap;
        justify-content: space-between;
        max-width: 1280px;
        margin: 0px auto;
        margin-top: 50px;
      }
    `}</style>
  </div>
)}

export default OmhSg

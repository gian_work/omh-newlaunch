import * as React from 'react'
import { useRouter } from 'next/router'
import PageLayout from '../layouts'
import '../styles/tailwind.css'

const routes = [
  "/[country]/[language]/property-calculator/affordability-calculator",
  "/[country]/[language]/property-calculator/buyers-stamp-duty",
  "/[country]/[language]/property-calculator/sellers-stamp-duty-calculator",
  "/[country]/[language]/new-property-launch/[slug]",
  "/[country]/[language]/home",
  "/[country]/[language]/prelaunch",
  "/[country]/[language]/newlaunches",
]

const Home = () => {
  const router = useRouter()
  React.useEffect(() => {
    if(router && router.pathname && routes.indexOf(router.pathname) === -1)
      location.href = 'https://omh.sg/'
  })

  return (
    <div>
    </div>
  )
}

export default PageLayout(Home, 'default')
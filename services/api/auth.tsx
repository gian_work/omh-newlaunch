import { Component } from 'react'
import Router from 'next/router'
import nextCookie from 'next-cookies'
import cookie from 'js-cookie'
import request from './request'
import API from './endpoints'
import { useEffect } from 'react'

const country = cookie.get('omh-country') || 'sg'
const lang = cookie.get('omh-language') || 'en'

export const setUserToken = (token: String) => {
  cookie.set('omh_cookie', token.split(" ")[1], { expires: 1 })
  Router.push(`/${country}/${lang}/profile`)
}

export const logout = () => {
  cookie.remove('omh_cookie')
  // to support logging out from all windows
  window.localStorage.setItem('logout', `${Date.now()}`)
  Router.push(`/${country}/${lang}/home`)
}


export const refreshToken = (bearer: String) => {
  return request({
    url: API.REFRESH,
    method: "POST",
    headers: {
      Authorization: `Bearer ${bearer}`
    }
  })
}

export const auth = ctx => {
  const { omh_cookie } = nextCookie(ctx)
  /*
   * If `ctx.req` is available it means we are on the server.
   * Additionally if there's no token it means the user is not logged in.
   */
  if(ctx.req && !omh_cookie) {
    ctx.res.writeHead(302, { Location: `/${country}/${lang}/home` })
    ctx.res.end()
  }

  // We already checked for server. This should only happen on client.
  if(!omh_cookie) {
    Router.push('/login')
  }

  return omh_cookie
}

export const withAuthSync = WrappedComponent => {
  const Wrapper = props => {
    const syncLogout = event => {
      if(event.key === 'logout') {
        console.log('logged out from storage!')
        Router.push('/login')
      }
    }

    useEffect(() => {
      window.addEventListener('storage', syncLogout)

      return () => {
        window.removeEventListener('storage', syncLogout)
        window.localStorage.removeItem('logout')
      }
    }, [null])

    return <WrappedComponent {...props} />
  }

  Wrapper.getInitialProps = async ctx => {
    const omh_cookie = auth(ctx)

    const componentProps =
      WrappedComponent.getInitialProps &&
      (await WrappedComponent.getInitialProps(ctx))

    return { ...componentProps, omh_cookie }
  }

  return Wrapper
}

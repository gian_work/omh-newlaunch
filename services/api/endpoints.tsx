const HOST = 'https://staging.ohmyhome.com/ms'

const API = {
  BASE_URL: 'https://staging.ohmyhome.com/ms',
  // ONBOARDING
  SIGNUP: '/onboarding/signup/mobile',
  VERIFY_MOBILE: '/onboarding/signup/mobile/verify',
  VERIFY_CONFIRM: '/onboarding/signup/mobile/verify/confirm',
  ADD_USER_ROLE: '/user/update/info',
  FACEBOOK: '/onboarding/signup/facebook',
  GOOGLE: '/onboarding/signup/google',
  // Login
  LOGIN_MOBILE: '/onboarding/login/mobile',
  LOGIN_FACEBOOK: '/onboarding/login/facebook',
  LOGIN_GOOGLE: '/onboarding/login/google',
  LOGIN_FORGOT_PASSWORD: '/onboarding/forget/password',
  // UPDATE USER PROFILE
  UPDATE_MOBILE: '/user/update/mobile',
  UPDATE_VALIDATE_MOBILE: '/user/update/mobile/validate',
  // AUTH
  REFRESH: '/user/refresh',
  // PROFILE
  PRESIGN_URL: '/upload/image/presign',
  PROCESS_IMAGE: '/upload/image/process',
  UPDATE_INFO: '/user/update/info',
}

export default API
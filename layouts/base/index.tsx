import React from 'react'

class BaseLayout extends React.Component {
  render() {
    const { children } = this.props
    return <div className='layout-base'>{children}</div>
  }
}

export default BaseLayout
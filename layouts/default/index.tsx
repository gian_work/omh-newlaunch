import React from 'react'

// Components
import Header from '../../components/site/header'
import Footer from '../../components/site/footer'

interface iProps {
  children: Object;
  hideFooter?: boolean;
}

const DefaultLayout: React.FunctionComponent<iProps> = ({
  children,
  hideFooter,
}) =>  {

  return (
    <div className='layout-default overflow-hidden'>
      <Header />
      {children}
      {!hideFooter &&
        <div>
          <Footer type="default" />
          <Footer type="seo" />
        </div>}
    </div>
  )
}

export default DefaultLayout
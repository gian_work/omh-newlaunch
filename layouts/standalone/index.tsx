import React from 'react'

// Components
import Header from '../../components/site/header'
import Footer from '../../components/site/footer'

const links = [
  {
    text: 'Find a Home',
    url: 'https://omh.sg/find-a-home',
    code: 'find_a_home'
  },
  {
    text: 'Post a Property',
    url: 'https://omh.sg/post-a-property',
    code: 'post_a_property'
  },
  {
    text: 'Get an Agent',
    url: 'https://omh.sg/get-an-agent',
    code: 'get_an_agent'
  },
  {
    text: 'Mortgage',
    url: 'https://omh.sg/services/mortgage?utm_source=inner&utm_campaign=header',
    code: 'mortgage'
  },
  {
    text: 'Services',
    url: 'https://omh.sg/services',
    code: 'services'
  }
]


function StandaloneLayout(props) {
  const { children } = props

  return (
    <div className='layout-default overflow-hidden'>
      <Header
        standalone={true}
        links={links}
      />
      <div className={'main__content--container'}>
        {children}
      </div>
      <div>
        <Footer 
          type="standalone" />
      </div>
    </div>
  )
}

export default StandaloneLayout
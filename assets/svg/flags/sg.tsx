import React from "react"

export default function FlagSg() {
  return (
    <span>
      <svg width="30" height="23" viewBox="0 0 30 23" fill="none" xmlns="http://www.w3.org/2000/svg">
        <g filter="url(#filter0_d)">
          <rect x="4" y="2" width="22" height="7.21925" fill="#FF3D00" />
          <rect x="4" y="9.05884" width="22" height="7.21925" fill="#ECEFF1" />
          <path fillRule="evenodd" clipRule="evenodd" d="M9.64384 8.02628C9.34484 8.12494 9.02584 8.17644 8.69134 8.17644C7.11534 8.17644 5.83334 6.99393 5.83334 5.52938C5.83334 4.06484 7.11534 2.88232 8.69134 2.88232C9.02584 2.88232 9.34484 2.93382 9.64384 3.03248C8.19984 3.07917 7.04934 4.18227 7.04934 5.52938C7.04934 6.8765 8.19984 7.97912 9.64384 8.02628Z" fill="#ECEFF1" />
          <path fillRule="evenodd" clipRule="evenodd" d="M9.04683 5.59343L8.65833 5.31766L8.27033 5.59343L8.41983 5.14872L8.03033 4.87439L8.51083 4.87536L8.65833 4.42969L8.80583 4.87536L9.28633 4.87439L8.89683 5.14872L9.04683 5.59343Z" fill="#ECEFF1" />
          <path fillRule="evenodd" clipRule="evenodd" d="M12.0933 5.59343L11.7048 5.31766L11.3168 5.59343L11.4663 5.14872L11.0768 4.87439L11.5573 4.87536L11.7048 4.42969L11.8523 4.87536L12.3333 4.87391L11.9438 5.14824L12.0933 5.59343Z" fill="#ECEFF1" />
          <path fillRule="evenodd" clipRule="evenodd" d="M10.5719 4.54607L10.1834 4.27029L9.79536 4.54607L9.94486 4.10136L9.55536 3.82703L10.0359 3.82799L10.1834 3.38232L10.3309 3.82799L10.8114 3.82703L10.4219 4.10136L10.5719 4.54607Z" fill="#ECEFF1" />
          <path fillRule="evenodd" clipRule="evenodd" d="M9.61684 7.26872L9.22834 6.99295L8.84034 7.26872L8.98984 6.82402L8.60034 6.54969L9.08084 6.55065L9.22834 6.10498L9.37584 6.55065L9.85634 6.54969L9.46684 6.82402L9.61684 7.26872Z" fill="#ECEFF1" />
          <path fillRule="evenodd" clipRule="evenodd" d="M11.5233 7.26872L11.1348 6.99295L10.7468 7.26872L10.8963 6.82402L10.5068 6.54969L10.9873 6.55065L11.1348 6.10498L11.2823 6.55065L11.7628 6.54969L11.3733 6.82402L11.5233 7.26872Z" fill="#ECEFF1" />
        </g>
        <defs>
          <filter id="filter0_d" x="0" y="0" width="30" height="22.2781" filterUnits="userSpaceOnUse" colorInterpolationFilters="sRGB">
            <feFlood floodOpacity="0" result="BackgroundImageFix" />
            <feColorMatrix in="SourceAlpha" type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0" />
            <feColorMatrix type="matrix" values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.122733 0" />
            <feBlend mode="normal" in2="BackgroundImageFix" result="effect1_dropShadow" />
            <feBlend mode="normal" in="SourceGraphic" in2="effect1_dropShadow" result="shape" />
          </filter>
        </defs>
      </svg>
      <style jsx>{`
        span {
          box-shadow: 0px 3px 10px -5px rgba(0,0,0,1);
        }
      `}</style>
    </span>
  )
}
